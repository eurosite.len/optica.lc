<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

?>
	<!--test3-->
<?if (IsModuleInstalled("advertising")):?>

<?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner",
	"bootstrap",
	array(
		"COMPONENT_TEMPLATE" => "bootstrap",
		"TYPE" => "MAIN",
		"NOINDEX" => "Y",
		"QUANTITY" => "3",
		"BS_EFFECT" => "fade",
		"BS_CYCLING" => "N",
		"BS_WRAP" => "Y",
		"BS_PAUSE" => "Y",
		"BS_KEYBOARD" => "Y",
		"BS_ARROW_NAV" => "Y",
		"BS_BULLET_NAV" => "Y",
		"BS_HIDE_FOR_TABLETS" => "N",
		"BS_HIDE_FOR_PHONES" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
	),
	false
);?>
<?endif?>
	<div class="benefits-section animatedParent animateOnce">

?><div class="animatedParent animateOnce">
	 <?
        global $filter_slider;
        $filter_slider=array('PROPERTY_VIEW'=>'Y');
        $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"sliders", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "sliders",
		"IBLOCK_ID" => "18",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "filter_slider",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "TITLE",
			1 => "URL",
			2 => "DESCRIPTION",
			3 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "sliders",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
</div>
<div class="benefits-section animatedParent animateOnce">
	<div class="container animated fadeInUpShort">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_superiority.php"
	)
);?>
	</div>
</div>
<div class="promo-section">
	<div class="row no-gutters animatedParent animateOnce">
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			<div class="about-section bg-image bg-image_left-bottom ">
 <img src="/local/templates/main/images/bg-box.jpg" alt="">
				<div class="about-section__holder">
					<div class="brand-logo">
 <img width="166" src="/local/templates/main/images/brand-logo.png" height="41" alt="">
					</div>
					<h1>контактные линзы<br>
					 Acuvue</h1>
					<p>
						 Всемирно известный бренд, давно завоевавший доверие у многих пользователей. Отличаются своей особенной мягкостью, способностью хорошо пропускать воздух и комфортностью при ношении
					</p>
 <a href="#" class="btn-white btn-large">Перейти в каталог</a>
				</div>
			</div>
		</div>
        <svg version="1.1" id="triangle" class="animated figure1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  width="124" height="160" viewBox="0 0 124.007 108.001" style="enable-background:new 0 0 124.007 108.001;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="path" transform="rotate(-180, 60.0194, 60.3266)" id="svg_5" d="m2.966791,110.247635l57.052565,-99.841972l57.052567,99.841972z" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff"/>
                                </g>
                            </g>
                        </svg>
        <svg version="1.1" id="dots" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="31" height="48" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="dot01 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                </g>
                            </g>
                        </svg>

        <div class="col-12 col-lg-6 animated fadeInUpShort">
                <? global $mainFilter;
                $mainFilter="";
                $APPLICATION->IncludeComponent(
	"bitrix:catalog.top", 
	"main_catalog_top", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"BASKET_URL" => "/personal/cart/index.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "mainFilter",
		"HIDE_NOT_AVAILABLE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "15",
		"IBLOCK_TYPE" => "1c_catalog",
		"LABEL_PROP" => array(
		),
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "Купить в 1 клик",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Купить",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "KOLICHESTVO_V_UPAKOVKE",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "KOLICHESTVO_V_UPAKOVKE",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "Типовое соглашение",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "KOLICHESTVO_V_UPAKOVKE",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
			0 => "KOLICHESTVO_V_UPAKOVKE",
		),
		"SECTION_URL" => "/catalog/#SECTION_CODE#",
		"SEF_MODE" => "N",
		"SHOW_CLOSE_POPUP" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SECTION",
		"COMPONENT_TEMPLATE" => "main_catalog_top"
	),
	false
);?>


</div>
	</div>
	<div class="row no-gutters flex-column-reverse flex-lg-row animatedParent animateOnce">
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			<div class="picking-section">
				<h3>Подобрать линзы</h3>
				<div class="picking-form">

					 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter", 
	"main_filter", 
	array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "main_filter",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"DISPLAY_ELEMENT_COUNT" => "Y",
		"FILTER_NAME" => "",
		"FILTER_VIEW_MODE" => "horizontal",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "15",
		"IBLOCK_TYPE" => "1c_catalog",
		"INSTANT_RELOAD" => "Y",
		"PAGER_PARAMS_NAME" => "arrPager",
		"POPUP_POSITION" => "left",
		"PRICE_CODE" => array(
			0 => "Типовое соглашение",
		),
		"SAVE_IN_SESSION" => "N",
		"SECTION_CODE" => "tovar",
		"SECTION_CODE_PATH" => "",
		"SECTION_DESCRIPTION" => "-",
		"SECTION_ID" => "",
		"SECTION_TITLE" => "-",
		"SEF_MODE" => "Y",
		"SEF_RULE" => "catalog/linzy/filter/#SMART_FILTER_PATH#/apply/",
		"SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
		"TEMPLATE_THEME" => "blue",
		"XML_EXPORT" => "N"
	),
	false
);?>


				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_bottom_right.php"
	)
);?>
		</div>
	</div>
</div>
<div class="main">
	<div class="container">
		<div class="articles-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_news.php"
	)
);?>
			</div>
			<div class="row animated fadeInUpShort">
				 <?

                global $filter_main_news;
                $filter_main_news=array('!PROPERTY_VIEW_MAIN_PAGE'=>false);
                $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_news", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "3",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "filter_main_news",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "VIEW_MAIN_PAGE",
			1 => "TOPIC",
			2 => "URL_PARTNERS",
			3 => "LOGO",
			4 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Полезные статьи",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "main_news",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
			</div>
		</div>
		<div class="reviews-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
 <span class="reviews-section__slogan">Покупайте контактные линзы, не выходя из дома</span>
				<h4>Отзывы покупателей</h4>
			</div>
			 <?global $review_block_filter;
            $review_block_filter = array(">PROPERTY_POST_REVIEW" => 0);
            ?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"block_reviews",
	Array(
		"ACTIVE_DATE_FORMAT" => "j M Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "block_reviews",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DATE_ACTIVE_FROM",3=>"DATE_CREATE",4=>"",),
		"FILTER_NAME" => "review_block_filter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "services",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "280",
		"PROPERTY_CODE" => array(0=>"POST_REVIEW",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
		<div class="partners-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_partners.php"
	)
);?>
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"partners",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "partners",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"ID",1=>"NAME",2=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "partners",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK" => "",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Партнеры",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"URL_PARTNERS",1=>"LOGO",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
	</div>
</div>
    <div class="modal-main">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">×</span>
            </div>
            <div class="modal-body">
                <div>Товар добавлен</div>
                <div>в корзину</div>
            </div>
            <div class="modal-footer">
            </div>
        </div>

    </div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>