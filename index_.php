<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
?><div class="animatedParent animateOnce">
	 <?
        global $filter_slider;
        $filter_slider=array('PROPERTY_VIEW'=>'Y');
        $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"sliders", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "sliders",
		"IBLOCK_ID" => "18",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "filter_slider",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "TITLE",
			1 => "URL",
			2 => "DESCRIPTION",
			3 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "sliders",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
</div>
<div class="benefits-section animatedParent animateOnce">
	<div class="container animated fadeInUpShort">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_superiority.php"
	)
);?>
	</div>
</div>
<div class="promo-section">
	<div class="row no-gutters animatedParent animateOnce">
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			<div class="about-section bg-image bg-image_left-bottom ">
 <img src="/local/templates/main/images/bg-box.jpg" alt="">
				<div class="about-section__holder">
					<div class="brand-logo">
 <img width="166" src="/local/templates/main/images/brand-logo.png" height="41" alt="">
					</div>
					<h1>контактные линзы<br>
					 Acuvue</h1>
					<p>
						 Всемирно известный бренд, давно завоевавший доверие у многих пользователей. Отличаются своей особенной мягкостью, способностью хорошо пропускать воздух и комфортностью при ношении
					</p>
 <a href="#" class="btn-white btn-large">Перейти в каталог</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			<div class="sales-hit animatedParent animateOnce">
 <span class="sales-hit__label">Количество линз в упаковке</span>
				<div class="amount-nav slider-nav">
					<div>
						 6
					</div>
					<div>
						 12
					</div>
					<div>
						 24
					</div>
					<div>
						 30
					</div>
				</div>
				<div class="hit-slider slider-for">
					<div>
						<div class="item-box">
							<div class="image-section">
 <a href="#"><img src="/local/templates/main/images/img03.jpg" alt=""></a>
							</div>
							<div class="text-box">
 <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
								<div class="price-wrap">
 <span class="price-wrap__amount">6 линз</span> <span class="price-wrap__sep"></span> <span class="price-wrap__price">870 <span class="price-value">i</span></span>
								</div>
								<div class="btns__footer">
 <a href="#" class="btn-blue btn-large">купить</a> <a href="#" class="btn-yellow btn-large">купить в 1 клик</a>
								</div>
							</div>
						</div>
					</div>
					<div>
						<div class="item-box">
							<div class="image-section">
 <a href="#"><img src="/local/templates/main/images/img03.jpg" alt=""></a>
							</div>
							<div class="text-box">
 <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
								<div class="price-wrap">
 <span class="price-wrap__amount">12 линз</span> <span class="price-wrap__sep"></span> <span class="price-wrap__price">1870 <span class="price-value">i</span></span>
								</div>
 <a href="#" class="btn-blue btn-large">Купить</a>
							</div>
						</div>
					</div>
					<div>
						<div class="item-box">
							<div class="image-section">
 <a href="#"><img src="/local/templates/main/images/img03.jpg" alt=""></a>
							</div>
							<div class="text-box">
 <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
								<div class="price-wrap">
 <span class="price-wrap__amount">24 линз</span> <span class="price-wrap__sep"></span> <span class="price-wrap__price">3870 <span class="price-value">i</span></span>
								</div>
 <a href="#" class="btn-blue btn-large">Купить</a>
							</div>
						</div>
					</div>
					<div>
						<div class="item-box">
							<div class="image-section">
 <a href="#"><img src="/local/templates/main/images/img03.jpg" alt=""></a>
							</div>
							<div class="text-box">
 <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
								<div class="price-wrap">
 <span class="price-wrap__amount">30 линз</span> <span class="price-wrap__sep"></span> <span class="price-wrap__price">6870 <span class="price-value">i</span></span>
								</div>
 <a href="#" class="btn-blue btn-large">Купить</a>
							</div>
						</div>
					</div>
				</div>
				<div class="view-more-wrap">
 <a href="#" class="btn-yellow">Посмотреть все</a>
				</div>
				<div class="animated-title animated">
 <a href="#" class="animated-title__text">хиты&nbsp;<span class="mark-blue">продаж</span></a>
					<div class="corner-box">
						<div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row no-gutters flex-column-reverse flex-lg-row animatedParent animateOnce">
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			<div class="picking-section">
				<h3>Подобрать линзы</h3>
				<div class="picking-form">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter",
                        "main_filter",
                        array(
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CONVERT_CURRENCY" => "N",
                            "DISPLAY_ELEMENT_COUNT" => "Y",
                            "FILTER_NAME" => "",
                            "FILTER_VIEW_MODE" => "horizontal",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "IBLOCK_ID" => "15",
                            "IBLOCK_TYPE" => "1c_catalog",
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "POPUP_POSITION" => "left",
                            "PRICE_CODE" => array(
                                0 => "Типовое соглашение",
                            ),
                            "SAVE_IN_SESSION" => "N",
                            "SECTION_CODE" => "#SECTION_CODE#",
                            "SECTION_DESCRIPTION" => "-",
                            "SECTION_ID" => "84",
                            "SECTION_TITLE" => "-",
                            "SEF_MODE" => "Y",
                            "TEMPLATE_THEME" => "blue",
                            "XML_EXPORT" => "N",
                            "COMPONENT_TEMPLATE" => "main_filter",
                            "SEF_RULE" => "catalog/#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
                            "SECTION_CODE_PATH" => "",
                            "SMART_FILTER_PATH" => ""
                        ),
                        false
                    );?>
					<form action="#">
						<div class="form-row">
 <span class="label">Цена, Р</span>
							<div class="form-box">
								<div id="range">
								</div>
							</div>
						</div>
						<div class="form-row">
 <span class="label">Срок ношения</span>
							<div class="form-box">
								<ul class="picking-list">
									<li> <input type="radio" class="picking__radio" name="1" id="picking__radio1"> <label for="picking__radio1">1 день</label> </li>
									<li> <input type="radio" class="picking__radio" checked="checked" name="1" id="picking__radio2"> <label for="picking__radio2">1 неделя</label> </li>
									<li> <input type="radio" class="picking__radio" name="1" id="picking__radio3"> <label for="picking__radio3">1 месяц</label> </li>
									<li> <input type="radio" class="picking__radio" name="1" id="picking__radio4"> <label for="picking__radio4">3 месяца</label> </li>
									<li> <input type="radio" class="picking__radio" name="1" id="picking__radio5"> <label for="picking__radio5">6 месяцев</label> </li>
								</ul>
								<div class="slide-holder">
									<div class="slide">
										<ul class="picking-list">
											<li> <input type="radio" class="picking__radio" name="1" id="picking__radio6"> <label for="picking__radio6">1 год</label> </li>
											<li> <input type="radio" class="picking__radio" name="1" id="picking__radio7"> <label for="picking__radio7">2 года</label> </li>
										</ul>
									</div>
 <a href="#" class="slide-holder__opener">Ещё<em>Скрыть</em></a>
								</div>
							</div>
						</div>
						<div class="form-row">
 <span class="label">Тип линз</span>
							<div class="form-box">
								<ul class="picking-list">
									<li> <input type="radio" class="picking__radio" checked="checked" name="2" id="picking__radio11"> <label for="picking__radio11">Прозрачные</label> </li>
									<li> <input type="radio" class="picking__radio" name="2" id="picking__radio12"> <label for="picking__radio12">Оттеночные</label> </li>
								</ul>
							</div>
						</div>
						<div class="form-row">
 <span class="label">Производитель</span>
							<div class="form-box">
								<ul class="picking-list">
									<li> <input type="radio" class="picking__radio" checked="checked" name="3" id="picking__radio13"> <label for="picking__radio13">Johnson &amp; Johnson</label> </li>
									<li> <input type="radio" class="picking__radio" name="3" id="picking__radio14"> <label for="picking__radio14">CIBA Vision</label> </li>
									<li> <input type="radio" class="picking__radio" name="3" id="picking__radio15"> <label for="picking__radio15">CooperVision</label> </li>
									<li> <input type="radio" class="picking__radio" name="3" id="picking__radio16"> <label for="picking__radio16">Bausch &amp; Lomb</label> </li>
								</ul>
								<div class="slide-holder">
									<div class="slide">
										<ul class="picking-list">
											<li> <input type="radio" class="picking__radio" name="3" id="picking__radio17"> <label for="picking__radio17">CooperVision</label> </li>
											<li> <input type="radio" class="picking__radio" name="3" id="picking__radio18"> <label for="picking__radio18">CooperVision</label> </li>
										</ul>
									</div>
 <a href="#" class="slide-holder__opener">Ещё<em>Скрыть</em></a>
								</div>
							</div>
						</div>
						<div class="submit-wrap">
 <button type="button" class="submit btn-blue btn-large">Подобрать</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6 animated fadeInUpShort">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_bottom_right.php"
	)
);?>
		</div>
	</div>
</div>
<div class="main">
	<div class="container">
		<div class="articles-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_news.php"
	)
);?>
			</div>
			<div class="row animated fadeInUpShort">
				 <?

                global $filter_main_news;
                $filter_main_news=array('!PROPERTY_VIEW_MAIN_PAGE'=>false);
                $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_news", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "3",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "filter_main_news",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "VIEW_MAIN_PAGE",
			1 => "TOPIC",
			2 => "URL_PARTNERS",
			3 => "LOGO",
			4 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Полезные статьи",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "main_news",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
			</div>
		</div>
		<div class="reviews-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
 <span class="reviews-section__slogan">Покупайте контактные линзы, не выходя из дома</span>
				<h4>Отзывы покупателей</h4>
			</div>
			 <?global $review_block_filter;
            $review_block_filter = array(">PROPERTY_POST_REVIEW" => 0);
            ?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"block_reviews",
	Array(
		"ACTIVE_DATE_FORMAT" => "j M Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "block_reviews",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DATE_ACTIVE_FROM",3=>"DATE_CREATE",4=>"",),
		"FILTER_NAME" => "review_block_filter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "services",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "280",
		"PROPERTY_CODE" => array(0=>"POST_REVIEW",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
		<div class="partners-section animatedParent animateOnce">
			<div class="h_center animated fadeInUpShort">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/main_partners.php"
	)
);?>
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"partners",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "partners",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"ID",1=>"NAME",2=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "partners",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK" => "",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Партнеры",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"URL_PARTNERS",1=>"LOGO",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>