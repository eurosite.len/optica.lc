$(document).ready(function() {
    // custom form
    jcf.replaceAll();


    // SLIDERS 

    $('.main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        customPaging : function(slider, i) {
            i++;
            return '0' + i;
        },
        fade: true,
        infinite: true,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.reviews-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        infinite: true,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.slider-nav',
        infinite: false

    });

    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true
    });

    $('.items-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.items-slider-nav',
        infinite: false

    });

    $('.char-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        infinite: false,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });

    $('.char-slider-mobile').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });

    $('.promo-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        centerMode: true,
        initialSlide: 2,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: false,
                    centerMode: false
                }
            }
        ]

    });

    $('.items-slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.items-slider',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.widget-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });

    $("[data-fancybox]").click(function() {
        $.fancybox.getInstance('close');
        $("[data-fancybox]").fancybox({
            touch: false,
            margin: [0],
            'beforeClose': function() {
                if (this.src == "#conditions") {
                    $.fancybox.getInstance('close');
                    $('a[data-src$="#question-modal"').trigger('click');
                }
            },
            'onClosed': function() { alert('closed'); }
        });
    });

    /* Calculates coordinates and places them in the image */
    function addParentBackground() {
        $('.bg-image').each(function() {
            if (typeof $(this).find('img').attr('src') != 'undefined') {
                $(this).css('background-image', 'url(' + $(this).find('img').attr('src') + ')');
            }
        });
    }

    /* Add parent background after loading */
    addParentBackground();

    // Range

    $("#range").ionRangeSlider({
        type: "double",
        min: 00,
        max: 2000,
        from: 300,
        to: 900,
        hide_min_max: true,
        hide_from_to: false,
        grid: false
    });

    //autocomplete

    var availableTags = [
      "Линзы",
      "Капли",
      "Растворы Avuvue"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });

    $(".catalog-item__amount a").on("click", function(e){
        e.preventDefault();
        $(this).parent().parent().find("a").removeClass("active");
        $(this).addClass("active");
    });



    //text field

    $(document).on('focus', '.text-field', function() {
        $(this).parent().addClass('text-focused');
    });

    $(document).on('focusout', '.text-field', function() {
        var thisPar = $(this).parent();
        setTimeout(function() {
            thisPar.removeClass('text-focused');
        }, 100);
    });

    $(document).on('keyup load blur', '.text-field', function() {
        var text = $(this).val();

        if ($(this).hasClass('phone-mask')) {
            if ($(this).val().replace(/[^0-9.]/g, '').length < 11) {
                text = "";
            }
        }

        if (text !== "") {
            $(this).parent().addClass('text-active');
        } else {
            $(this).parent().removeClass('text-active');
        }
    });

    $(".phone-mask").mask("+7 (999) 999-9999");

     $(".calc-box .calc-box__check-list li .check").change(function(){
            if($(this).closest(".calc-box .calc-box__check-list li:first-child .check").is(":checked")) {
                $(this).closest(".calc-box").addClass("calc-box_active");
            } else {
                $(this).closest(".calc-box").removeClass("calc-box_active");
            }
    });

     $(".order-tabs__delivery-section .order-tabs__delivery-row .order-tabs__delivery-col .check").change(function(){
            if($(this).closest(".order-tabs__delivery-section .order-tabs__delivery-row  .order-tabs__delivery-col:first-child .check").is(":checked")) {
                $(this).closest(".order-tabs__delivery-section").addClass("order-tabs__delivery-section_active");
            } else {
                $(this).closest(".order-tabs__delivery-section").removeClass("order-tabs__delivery-section_active");
            }
    });

     $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scroll-top').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    function TotalLength(){
        var path = document.querySelector('#article-set2 .pie1');
        var len = Math.round(path.getTotalLength());
        console.log("Длина  " + len);
    };

    //TotalLength();

    $(document).on('click', 'a.anchor-link[href^="#"]', function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 71
        }, 500);
    });

});

