ymaps.ready(init);

function init(){     

    var myMap;
    var myMap2;

    myMap = new ymaps.Map("map", {
        center: [55.7355873, 37.5941313],
        zoom: 17,
        controls: []
    });
    
    myMap.behaviors.disable('scrollZoom');
    
    myMap.controls.add("zoomControl", {
        position: {top: 15, left: 15}
    });
    
    var myPlacemark = new ymaps.Placemark([55.7355873, 37.5941313] , {},
        { iconLayout: 'default#image',
          iconImageHref: './images/ico-marker.png',
          iconImageSize: [39, 51],
          iconImageOffset: [-19, -51] });     

    myMap.geoObjects.add(myPlacemark);

    myMap2 = new ymaps.Map("map2", {
        center: [55.7355873, 37.5941313],
        zoom: 17,
        controls: []
    });
    
    myMap2.behaviors.disable('scrollZoom');
    
    myMap2.controls.add("zoomControl", {
        position: {top: 15, left: 15}
    });
    
    var myPlacemark2 = new ymaps.Placemark([55.7355873, 37.5941313] , {},
        { iconLayout: 'default#image',
          iconImageHref: './images/ico-marker.png',
          iconImageSize: [39, 51],
          iconImageOffset: [-19, -51] });     

    myMap2.geoObjects.add(myPlacemark2);
}