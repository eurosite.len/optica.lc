$(document).ready(function() {

    // cart calculation
    $('.minus').click(function(e) {
        e.preventDefault();

        var $input = $(this).parent().find('input');
        var value = $.isNumeric($input.val()) ? $input.val() : 0;

        var count = parseInt(value) - 1;

        count = count < 1 ? 1 : count;

        $input.val(count);

        $input.change();

        return false;

    });

    $('.plus').click(function(e) {
        e.preventDefault();

        var $input = $(this).parent().find('input');
        var value = $.isNumeric($input.val()) ? $input.val() : 0;

        $input.val(parseInt(value) + 1);

        $input.change();

        return false;

    });

    // фильтрация ввода в поля
    jQuery('.amount-wrap .text, .calc-box__amount .text').keypress(function(event) {
        var key, keyChar;
        if (!event) var event = window.event;

        if (event.keyCode) key = event.keyCode;
        else if (event.which) key = event.which;

        if (key == null || key == 0 || key == 8 || key == 13 || key == 9 || key == 46 || key == 37 || key == 39) return true;
        keyChar = String.fromCharCode(key);

        if (!/\d/.test(keyChar)) return false;

    });


});