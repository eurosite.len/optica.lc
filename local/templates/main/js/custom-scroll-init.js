$(document).ready(function() {
    $(".scroll-horizontal").mCustomScrollbar({
        axis: "x",
        autoExpandScrollbar: true,
        advanced: { autoExpandHorizontalScroll: true }
    });

    $(".scroll-vertical").mCustomScrollbar({
        axis: "y",
        advanced: { autoScrollOnFocus: [".main-menu"] }
    });

    $('.item-same').matchHeight({
        byRow: false,
        property: 'height',
        target: null,
        remove: false
    });

    $('.item-same2').matchHeight({
        byRow: false,
        property: 'height',
        target: null,
        remove: false
    });
});