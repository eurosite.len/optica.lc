<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if($APPLICATION->GetCurPage()!='/index.php'):?>
           </div>
       </div>
<?endif;?>
<footer class="footer">
        <div class="container">
            <div class="subscribe-section">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.edit",
                    "ditrade.subs",
                    array(
                        "COMPONENT_TEMPLATE" => "ditrade.subs",
                        "SHOW_HIDDEN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "ALLOW_ANONYMOUS" => "Y",
                        "SHOW_AUTH_LINKS" => "Y",
                        "SET_TITLE" => "N"
                    ),
                    false
                );?>
            </div>
            <ul class="footer-nav">
                <li><a href="#">Контактные линзы</a></li>
                <li><a href="#">Растворы</a></li>
                <li><a href="#">Капли</a></li>
                <li><a href="#">Аксессуары</a></li>
                <li><a href="#">Распродажа</a></li>
            </ul>
            <div class="payment-box">
                <span class="payment-title">Мы принимаем к оплате</span>
                <ul class="payment-list">
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment01.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment02.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment03.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment04.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment05.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment06.png" alt=""></a></li>
                    <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/payment07.png" alt=""></a></li>
                </ul>
            </div>
            <div class="copy-wrap">
                <span class="copy">&copy; 2018 DITRADE. Все права защищены</span>
                <span class="separator"></span>
                <span href="https://www.extyl-pro.ru" class="created">Сделано в <a href="https://www.extyl-pro.ru">Extyl-Pro</a></span>
            </div>
        </div>
    </footer>
</div>
<!-- Modal -->

<div class="modal-window" id="login-popup">
    <div class="row no-gutters">
        <div class="col-md-4 col-12 login-col">
            <div class="login-tabs-wrap bg-image">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/img08.jpg" alt="">
                <ul class="login-tabs">
                    <li><a href="#" class="active">войти<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                    <li><a href="#reg-popup" data-fancybox>регистрация<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md col-12">
            <div class="login-content">
                <div class="main-form main-form_modal">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "main",
                            Array(
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => "",
                                "REGISTER_URL" => "",
                                "SHOW_ERRORS" => "N"
                            )
                        );?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-window" id="reg-popup">
    <div class="row no-gutters">
        <div class="col-md-4 col-12 login-col">
            <div class="login-tabs-wrap bg-image">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/img08.jpg" alt="">
                <ul class="login-tabs">
                    <li><a href="#login-popup" data-fancybox>войти<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                    <li><a href="#" class="active">регистрация<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md col-12">
            <div class="login-content">
                <div class="main-form main-form_modal">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.register",
                        "main",
                        Array(
                            "AUTH" => "Y",
                            "REQUIRED_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_PHONE"),
                            "SET_TITLE" => "N",
                            "SHOW_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_PHONE"),
                            "SUCCESS_PAGE" => "",
                            "USER_PROPERTY" => array(),
                            "USER_PROPERTY_NAME" => "",
                            "USE_BACKURL" => "Y"
                        )
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-window" id="callback-popup">
    <div class="row no-gutters">
        <div class="col-md-4 col-12 login-col">
            <div class="login-tabs-wrap bg-image">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/img08.jpg" alt="">
                <span class="callback-modal-title">Оставить заявку</span>
            </div>
        </div>
        <div class="col-md col-12">
            <div class="login-content">
                <div class="main-form main-form_modal"><?$APPLICATION->IncludeComponent(
                        "bitrix:form.result.new",
                        "callback",
                        array(
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "CHAIN_ITEM_LINK" => "",
                            "CHAIN_ITEM_TEXT" => "",
                            "EDIT_URL" => "",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "LIST_URL" => "",
                            "SEF_MODE" => "N",
                            "SUCCESS_URL" => "",
                            "USE_EXTENDED_ERRORS" => "N",
                            "WEB_FORM_ID" => "2",
                            "COMPONENT_TEMPLATE" => "callback",
                            "VARIABLE_ALIASES" => array(
                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                "RESULT_ID" => "RESULT_ID",
                            )
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-window" id="forgot-popup">
    <div class="row no-gutters">
        <div class="col-md-4 col-12 login-col">
            <div class="login-tabs-wrap bg-image">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/img08.jpg" alt="">
                <ul class="login-tabs">
                    <li><a href="#login-popup" data-fancybox>войти<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                    <li><a href="#reg-popup" data-fancybox>регистрация<div class="corner-box corner-box_yellow"><div></div></div></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md col-12">
            <div class="login-content">
                <div class="main-form main-form_modal">
                    <?if($_REQUEST['change_password']=='yes'):?>
                        <?$APPLICATION->IncludeComponent(
                        "bitrix:system.auth.changepasswd",
                        "main",
                        Array()
                        );?>
                    <?else:?>
                        <?$APPLICATION->IncludeComponent(
                        "bitrix:system.auth.forgotpasswd",
                        "main",
                        Array()
                        );?>
                    <?endif;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-window" id="add_review-popup">
    <div class="row no-gutters">
        <div class="col-md-4 col-12 login-col">
            <div class="login-tabs-wrap bg-image">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/img08.jpg" alt="">
            </div>
        </div>
        <div class="col-md col-12">
            <div class="login-content">
                <h4>Оставить отзыв</h4>
                <div class="main-form main-form_modal">
                    <?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form", 
	"add_review", 
	array(
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "Имя",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "Отзыв",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(
			0 => "2",
		),
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "services",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
		),
		"RESIZE_IMAGES" => "N",
		"SEF_MODE" => "N",
		"STATUS" => "INACTIVE",
		"STATUS_NEW" => "ANY",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "add_review"
	),
	false
);?>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="#" class="scroll-top"></a>


</body>

</html>
