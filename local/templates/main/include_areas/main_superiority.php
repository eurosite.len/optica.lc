<ul class="benefits-list">
	<li>
	<div class="icon-holder">
 <img alt="Бесплатный подбор линз" src="/local/templates/main/images/ico-benefit01.png">
		<div class="corner-box corner-box_yellow">
			<div>
			</div>
		</div>
	</div>
 <span class="benefits-list__title">Бесплатный подбор линз </span> <span class="benefits-list__text">и обучение их использованию</span> </li>
	<li>
	<div class="icon-holder">
 <img alt="Быстрая доставка" src="/local/templates/main/images/ico-benefit02.png">
		<div class="corner-box corner-box_yellow">
			<div>
			</div>
		</div>
	</div>
 <span class="benefits-list__title">Быстрая доставка</span> <span class="benefits-list__text">в течение 3 часов</span> </li>
	<li>
	<div class="icon-holder">
 <img alt="Гарантия высокого качеств" src="/local/templates/main/images/ico-benefit03.png">
		<div class="corner-box corner-box_yellow">
			<div>
			</div>
		</div>
	</div>
 <span class="benefits-list__title">Гарантия высокого качества</span> <span class="benefits-list__text">на нашу продукцию</span> </li>
</ul>