<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
$min_price=array_reduce($item['OFFERS'],function ($carry, $item){
    $type=key (array_slice($item['PRICES'],0,1));
    $price=array_shift(array_slice($item['PRICES'],0,1));
    $price['TYPE_PRICE']=$type;
    if(empty($carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]))
    {
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]=$item;
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']=$price['VALUE'];}

    if($carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']<$price['VALUE']){
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]=$item;
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']=$price['VALUE'];
    }
    return $carry;});

$arr_url=explode("/",$item['DETAIL_PAGE_URL']);

$category_url="/".$arr_url[1]."/".$arr_url[2]."/";

ksort($min_price);
reset($min_price);
$first_offer=array_slice($min_price,0,1)[0];
$first_count=key(array_slice($min_price,0,1,true));
?>


    <div class="catalog-item" data-id_product="<?=$item['ID']?>" data-id_offer="<?=$first_offer['ID']?>">
        <div class="corner-box"><div></div></div>
        <div class="catalog-item__holder">
            <ul class="catalog-item__amount catalog-item__amount_vertical">
<?
if(!empty($first_count)):?>
                <li><a class="active"><?=$first_count?></a></li>
    <?endif?>
                    <?foreach(array_slice($min_price,1,true) as $key =>$offers)
                    {?>
                        <li><a><?=$key?></a></li>
                    <?}?>
            </ul>

            <div class="image-section">
                <a href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['NAME']?>"></a>
            </div>
            <div class="text-box">
                <a href="#" class="title"><?=$item['NAME']?></a>
                <div class="price-wrap">
                    <span class="price-wrap__amount"><?=$first_count?> линз</span>
                    <span class="price-wrap__sep"></span>
                    <div class="price-wrap__price-box">
                        <?/*<span class="price-wrap__old-price">870 <span class="price-value">i</span></span>*/?>
                        <span class="price-wrap__price"><?=$first_offer['PRICE']?><span class="price-value">i</span></span>
                        <?/*<span class="price-wrap__day-payment">114 ₽ в день</span>*/?>
                    </div>
                </div>
                <ul class="options-list">
                    <?/*<li class="tooltip-parent">
                        <a class="gift"><div class="tooltip tooltip-right">К этому товару предлагается подарок</div></a>
                    </li>*/?>
                    <li class="tooltip-parent">
                        <a class="compare"><div class="tooltip tooltip-right">Добавить в сравнение</div></a>
                    </li>
<?/*<li class="tooltip-parent">
                        <span class="sale">15% <div class="tooltip tooltip-right">Акционный товар</div></span>
                    </li>*/?>
                </ul>
                <div class="btns__footer">
                    <a class="btn-blue btn-large btn-buy" style="cursor:pointer"><span>купить</span></a>
                    <a class="btn-yellow btn-large btn-buy_one" style="cursor:pointer"><span>купить в 1 клик</span></a>
                </div>
            </div>
        </div>
    </div>




<?/*
<span class="sales-hit__label">Количество линз в упаковке</span>
<div class="amount-nav slider-nav">
   <? foreach($min_price as $key =>$offers)
    {?>
    <div><span><?=$key?></span></div>
    <?}?>
</div>
<div class="hit-slider slider-for">
<?
foreach($min_price as $key =>$offers)
{   $price=array_shift(array_slice($offers['PRICES'],0,1));
    $link=ltrim($offers['~BUY_URL'],'/?');
?>
    <div>
        <div class="item-box">
            <div class="image-section">
                <a href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['NAME']?>"></a>
            </div>
            <div class="text-box">
            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="title"><?=$item['NAME']?></a>
            <div class="price-wrap">
                <span class="price-wrap__amount"><?=$key?> линз</span>
                <span class="price-wrap__sep"></span>
                <span class="price-wrap__price"><?=$price['VALUE']?> <span class="price-value">i</span></span>
            </div>
            <div class="btns__footer">
                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="btn-blue btn-large"><span>Купить</span></a>
    <?
    if(!empty($link)){
    ?>
    <a style="cursor:pointer" class="btn-yellow btn-large" id="link_buy_<?=$item['ID']?>_<?=$key?>"><span>Купить в 1 клик</span></a>
<script>


    $('span.close').on('click', function(){
        $('.modal').css('display','none');
    })


    $(window).on('click', function(event){
        if (jQuery.inArray( event.target, $('.modal') ) != "-1") {
            $('.modal').css('display','none');
        }
    })

    $("#link_buy_<?=$item['ID']?>_<?=$key?>").on('click',function(){
        $.get('/','<?=$link?>').done(function (result) {

            $('#myModal2').css('display','block');

        });
    });
</script>

<?}?>
            </div>
            </div>
    </div>
    </div>
<?}?>

</div>

<div class="view-more-wrap">
    <a href="<?=$category_url?>" class="btn-yellow"><span>Посмотреть все</span></a>
</div>
<div class="animated-title animated">
    <a href="#" class="animated-title__text">хиты&nbsp;<span class="mark-blue">продаж</span></a>
    <div class="corner-box"><div></div></div>
</div>

<? */