<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
$min_price=array_reduce($item['OFFERS'],function ($carry, $item){
    $type=key (array_slice($item['PRICES'],0,1));
    $price=array_shift(array_slice($item['PRICES'],0,1));
    $price['TYPE_PRICE']=$type;
    if(empty($carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]))
    {
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]=$item;
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']=$price['VALUE'];}

    if($carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']<$price['VALUE']){
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]=$item;
        $carry[$item['PROPERTIES']['CML2_ATTRIBUTES']['VALUE'][0]]['PRICE']=$price['VALUE'];
    }
    return $carry;});

$arr_url=explode("/",$item['DETAIL_PAGE_URL']);

$category_url="/".$arr_url[1]."/".$arr_url[2]."/";

?>

<svg version="1.1" id="dots2" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="150" height="150" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="dot01 dot dot-l" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot07 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot08 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot09 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot10 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot11 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot12 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                </g>
                            </g>
                        </svg>

<svg id="lines" class="animated" xmlns="http://www.w3.org/2000/svg" width="100" height="36" viewBox="0 0 95.85 31.99">
    <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,5.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,8.72,1.26,11-8.56,20.88-8.5C58.5,2.78,60.86,13,69.62,11.85c7.61-1,9.85-9.32,18-9.25A14.78,14.78,0,0,1,96,5.6"/>
    <path class="path2" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,14.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,22,69.62,20.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
    <path class="path3" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,24.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,32,69.62,30.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
</svg>
<svg version="1.1" id="vertical-line" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 120" style="enable-background:new 0 0 18 120;" xml:space="preserve" width="20" height="122">
                            <g>
                                <g>
                                    <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M17.218,30.663c0.602-0.757,1.204-1.589,0.377-2.27
                                        l-7.14-6.058c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272L6.1,0H1.741l6.085,5.149
                                        c0.827,0.68,0.152,1.663-0.374,2.27l-6.988,8.027c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                        c0.751,0.606,0.152,1.666-0.374,2.196L0.464,33.92c-0.753,0.909-0.526,1.59,0.225,2.196l7.137,6.058
                                        c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119l7.365,6.396
                                        c0.335,0.64-0.159,1.41-0.602,1.922l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                        c0.751,0.606,0.152,1.666-0.374,2.196L0.464,89.41c-0.753,0.91-0.526,1.59,0.225,2.196l7.137,6.058
                                        c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119L12.109,120h4.283l-6.085-5.299
                                        c-0.751-0.606-0.976-1.287-0.225-2.196l6.988-8.025c0.602-0.683,1.352-1.363,0.525-2.046l-7.14-6.056
                                        c-0.751-0.606-1.125-1.44-0.225-2.196l6.988-8.027c0.602-0.757,1.204-1.589,0.377-2.27l-7.14-6.058
                                        c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272l-7.77-6.653
                                        c-0.306-0.475-0.284-1.01,0.256-1.666l6.988-8.026c0.602-0.682,1.352-1.363,0.525-2.046l-7.14-6.056
                                        c-0.751-0.606-1.125-1.439-0.225-2.196L17.218,30.663z"/>
                                </g>
                            </g>
                        </svg>
<span class="sales-hit__label">Количество линз в упаковке</span>
<div class="amount-nav slider-nav">
   <? foreach($min_price as $key =>$offers)
    {?>
    <div><span><?=$key?></span></div>
    <?}?>
</div>
<div class="hit-slider slider-for">
<?
foreach($min_price as $key =>$offers)
{   $price=array_shift(array_slice($offers['PRICES'],0,1));
    $link=ltrim($offers['~BUY_URL'],'/?');
?>
    <div>
        <div class="item-box">
            <div class="image-section">
                <a href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['NAME']?>"></a>
            </div>
            <div class="text-box">
            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="title"><?=$item['NAME']?></a>
            <div class="price-wrap">
                <span class="price-wrap__amount"><?=$key?> линз</span>
                <span class="price-wrap__sep"></span>
                <span class="price-wrap__price"><?=$price['VALUE']?> <span class="price-value">i</span></span>
            </div>
            <div class="btns__footer">
                <a class="btn-blue btn-large" id="link_buy_<?=$item['ID']?>_<?=$key?>"><span>Купить</span></a>
    <?
    if(!empty($link)){
    ?>
    <a style="cursor:pointer" class="btn-yellow btn-large" ><span>Купить в 1 клик</span></a>
<script>

    $("#link_buy_<?=$item['ID']?>_<?=$key?>").on('click',function(){
        $.get('/','<?=$link?>').done(function (result) {

            $('.modal-main').css('display','block');

        });
    });
</script>

<?}?>
            </div>
            </div>
    </div>
    </div>
<?}?>

</div>

<div class="view-more-wrap">
    <a href="<?=$category_url?>" class="btn-yellow"><span>Посмотреть все</span></a>
</div>
<div class="animated-title animated">
    <a href="#" class="animated-title__text">хиты&nbsp;<span class="mark-blue">продаж</span></a>
    <div class="corner-box"><div></div></div>
</div>

