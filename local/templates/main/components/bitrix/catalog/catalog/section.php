<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
//todo ломает верстку
//$this->setFrameMode(true);
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter)
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
		$arCurSection = array();
}
?>
<div class="twocolumns animatedParent animateOnce">
    <svg version="1.1" id="catalog-set" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 214 240" xml:space="preserve" width="212" height="240">
                        <g>
                            <g>
                                <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M17.218,30.663c0.602-0.757,1.204-1.589,0.377-2.27
                                        l-7.14-6.058c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272L6.1,0H1.741l6.085,5.149
                                        c0.827,0.68,0.152,1.663-0.374,2.27l-6.988,8.027c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                        c0.751,0.606,0.152,1.666-0.374,2.196L0.464,33.92c-0.753,0.909-0.526,1.59,0.225,2.196l7.137,6.058
                                        c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119l7.365,6.396
                                        c0.335,0.64-0.159,1.41-0.602,1.922l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                        c0.751,0.606,0.152,1.666-0.374,2.196L0.464,89.41c-0.753,0.91-0.526,1.59,0.225,2.196l7.137,6.058
                                        c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119L12.109,120h4.283l-6.085-5.299
                                        c-0.751-0.606-0.976-1.287-0.225-2.196l6.988-8.025c0.602-0.683,1.352-1.363,0.525-2.046l-7.14-6.056
                                        c-0.751-0.606-1.125-1.44-0.225-2.196l6.988-8.027c0.602-0.757,1.204-1.589,0.377-2.27l-7.14-6.058
                                        c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272l-7.77-6.653
                                        c-0.306-0.475-0.284-1.01,0.256-1.666l6.988-8.026c0.602-0.682,1.352-1.363,0.525-2.046l-7.14-6.056
                                        c-0.751-0.606-1.125-1.439-0.225-2.196L17.218,30.663z"/>
                                <path class="line1" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff" d="M1.85,125.28,52.22,3.79,170.33,142.92,182,99.59""></path>
                                <circle class="pie1" cx="16" cy="16" r="8"/>
                            </g>
                        </g>
                    </svg>
    <svg version="1.1" id="catalog-set2" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 180 150" xml:space="preserve" width="180" height="150">
                        <path class="line1" fill-opacity="1" stroke-opacity="null" stroke-width="1" stroke="#000" fill="rgba(255,255,255,0)" d="M173.358,47.097c-0.907,0-1.642,0.733-1.642,1.638
            c0,50.718-41.341,91.979-92.158,91.979c-41.506,0-75.275-33.702-75.275-75.127c0-33.806,27.558-61.31,61.43-61.31
            c27.612,0,50.077,22.421,50.077,49.979c0,22.436-18.289,40.689-40.768,40.689c-18.27,0-33.134-14.835-33.134-33.07
            c0-14.79,12.057-26.822,26.876-26.822c11.988,0,21.741,9.734,21.741,21.698c0,9.649-7.865,17.498-17.532,17.498
            c-7.764,0-14.081-6.305-14.081-14.054c0-6.192,5.047-11.23,11.251-11.23c4.924,0,8.931,3.999,8.931,8.914
            c0,0.905,0.735,1.638,1.642,1.638c0.907,0,1.641-0.733,1.641-1.638c0-6.722-5.479-12.19-12.214-12.19
            c-8.014,0-14.534,6.508-14.534,14.506c0,9.556,7.79,17.33,17.364,17.33c11.477,0,20.816-9.32,20.816-20.775
            c0-13.771-11.226-24.975-25.024-24.975c-16.629,0-30.159,13.502-30.159,30.098c0,20.042,16.336,36.346,36.417,36.346
            c24.289,0,44.051-19.723,44.051-43.966C119.073,24.89,95.135,1,65.713,1C30.03,1,1,29.974,1,65.587
            c0,43.232,35.241,78.404,78.558,78.404c52.627,0,95.441-42.731,95.441-95.255C174.999,47.83,174.264,47.097,173.358,47.097z"></path>
                    </svg>
<?
//if ($isVerticalFilter)
	include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_vertical.php");
//else
//	include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_horizontal.php");
?>
</div>