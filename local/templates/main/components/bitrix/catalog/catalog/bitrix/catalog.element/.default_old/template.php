
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'ITEM' => array(
        'ID' => $arResult['ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
        'JS_OFFERS' => $arResult['JS_OFFERS']
    )
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
    'ID' => $mainId,
    'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
    'STICKER_ID' => $mainId.'_sticker',
    'BIG_SLIDER_ID' => $mainId.'_big_slider',
    'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
    'SLIDER_CONT_ID' => $mainId.'_slider_cont',
    'OLD_PRICE_ID' => $mainId.'_old_price',
    'PRICE_ID' => $mainId.'_price',
    'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
    'PRICE_TOTAL' => $mainId.'_price_total',
    'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
    'QUANTITY_ID' => $mainId.'_quantity',
    'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
    'QUANTITY_UP_ID' => $mainId.'_quant_up',
    'QUANTITY_MEASURE' => $mainId.'_quant_measure',
    'QUANTITY_LIMIT' => $mainId.'_quant_limit',
    'BUY_LINK' => $mainId.'_buy_link',
    'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
    'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
    'COMPARE_LINK' => $mainId.'_compare_link',
    'TREE_ID' => $mainId.'_skudiv',
    'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
    'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
    'OFFER_GROUP' => $mainId.'_set_group_',
    'BASKET_PROP_DIV' => $mainId.'_basket_prop',
    'SUBSCRIBE_LINK' => $mainId.'_subscribe',
    'TABS_ID' => $mainId.'_tabs',
    'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
    'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
    'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);

?>


<?
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>





<div class="item-info animatedParent animateOnce">
    <svg version="1.1" id="triangle" class="animated figure1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  width="124" height="160" viewBox="0 0 124.007 108.001" style="enable-background:new 0 0 124.007 108.001;" xml:space="preserve">
                        <g>
                            <g>
                                <path class="path" transform="rotate(-180, 60.0194, 60.3266)" id="svg_5" d="m2.966791,110.247635l57.052565,-99.841972l57.052567,99.841972z" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff"/>
                            </g>
                        </g>
                    </svg>
    <svg version="1.1" id="dots" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="31" height="48" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                        <g>
                            <g>
                                <path class="dot01 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                            </g>
                        </g>
                    </svg>
    <svg version="1.1" id="dots2" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="150" height="150" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="dot01 dot dot-l" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot07 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot08 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot09 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot10 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot11 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot12 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                </g>
                            </g>
                    </svg>
    <svg id="lines" class="animated" xmlns="http://www.w3.org/2000/svg" width="100" height="36" viewBox="0 0 95.85 31.99">
        <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,5.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,8.72,1.26,11-8.56,20.88-8.5C58.5,2.78,60.86,13,69.62,11.85c7.61-1,9.85-9.32,18-9.25A14.78,14.78,0,0,1,96,5.6"/>
        <path class="path2" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,14.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,22,69.62,20.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
        <path class="path3" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,24.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,32,69.62,30.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
    </svg>
    <svg version="1.1" id="vertical-line" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 120" style="enable-background:new 0 0 18 120;" xml:space="preserve" width="20" height="122">
                        <g>
                            <g>
                                <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M17.218,30.663c0.602-0.757,1.204-1.589,0.377-2.27
                                    l-7.14-6.058c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272L6.1,0H1.741l6.085,5.149
                                    c0.827,0.68,0.152,1.663-0.374,2.27l-6.988,8.027c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                    c0.751,0.606,0.152,1.666-0.374,2.196L0.464,33.92c-0.753,0.909-0.526,1.59,0.225,2.196l7.137,6.058
                                    c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119l7.365,6.396
                                    c0.335,0.64-0.159,1.41-0.602,1.922l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                    c0.751,0.606,0.152,1.666-0.374,2.196L0.464,89.41c-0.753,0.91-0.526,1.59,0.225,2.196l7.137,6.058
                                    c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119L12.109,120h4.283l-6.085-5.299
                                    c-0.751-0.606-0.976-1.287-0.225-2.196l6.988-8.025c0.602-0.683,1.352-1.363,0.525-2.046l-7.14-6.056
                                    c-0.751-0.606-1.125-1.44-0.225-2.196l6.988-8.027c0.602-0.757,1.204-1.589,0.377-2.27l-7.14-6.058
                                    c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272l-7.77-6.653
                                    c-0.306-0.475-0.284-1.01,0.256-1.666l6.988-8.026c0.602-0.682,1.352-1.363,0.525-2.046l-7.14-6.056
                                    c-0.751-0.606-1.125-1.439-0.225-2.196L17.218,30.663z"/>
                            </g>
                        </g>
                    </svg>

    <svg version="1.1" id="lines3" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 410 46" style="enable-background:new 0 0 18 120;" xml:space="preserve" width="410" height="46">
                        <g>
                            <g>
                                <circle class="pie1" cx="0" cy="0" r="4"/>
                                <path class="line1"  fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff" d="M0,1H370"></path>
                                <path class="line2"  fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff" d="M0,1H370"></path>
                            </g>
                        </g>
                    </svg>
    <a href="#" class="gift-link hidden-md-down animated fadeInUpShort tooltip-parent"><span class="tooltip">к этому товару предлагается подарок</span></a>
    <div class="row no-gutters animated fadeInUpShort">
        <div class="col-12 col-md-5 item-info__contacts">
            <span class="item-info__presence">Товар доступен только в нашем магазине</span>
            <span class="item-info__article">Артикул: 675467</span>
            <span class="item-info__label">Количество линз в упаковке</span>
            <ul class="catalog-item__amount">
                <li><a href="#" class="active">6</a></li>
                <li><a href="#">12</a></li>
                <li><a href="#">24</a></li>
                <li><a href="#">30</a></li>
            </ul>
            <span class="item-info__label">Консультация по телефону</span>
            <a href="#" class="item-info__phone">8 (495) 275 44 00</a>
            <div class="view-all-chars">
                <a href="#char-section" class="anchor-link btn-blue"><span>Все характеристики</span></a>

            </div>
        </div>
        <div class="col-12 col-md item-info__slider-wrap">
            <a href="#" class="gift-link hidden-lg-up"></a>
            <div class="items-slider">
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09.jpg" alt="">
                    </div>
                </div>
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09-1.jpg" alt="">
                    </div>
                </div>
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09.jpg" alt="">
                    </div>
                </div>
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09-1.jpg" alt="">
                    </div>
                </div>
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09.jpg" alt="">
                    </div>
                </div>
                <div>
                    <div class="small zoom-item">
                        <img src="images/img09-1.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="items-slider-nav">
                <div>
                    <div class="holder">
                        <img src="images/img09.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
                <div>
                    <div class="holder">
                        <img src="images/img10.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
                <div>
                    <div class="holder">
                        <img src="images/placeholder.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
                <div>
                    <div class="holder">
                        <img src="images/img09.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
                <div>
                    <div class="holder">
                        <img src="images/img10.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
                <div>
                    <div class="holder">
                        <img src="images/img11.jpg" alt="">
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg item-info__calc">
            <div class="calc-box">
                <div class="corner-box"><div></div></div>
                <ul class="calc-box__check-list">
                    <li>
                        <div class="check-wrap check-wrap_type">
                            <input type="radio" class="check" id="checkbox02" name="1">
                            <label for="checkbox02">одинаковые</label>
                        </div>
                    </li>
                    <li>
                        <div class="check-wrap check-wrap_type">
                            <input type="radio" class="check" checked="checked" id="checkbox03" name="1">
                            <label for="checkbox03">различающиеся</label>
                        </div>
                    </li>
                </ul>
                <div class="calc-box__tab1">
                    <div class="calc-box__row">
                        <span class="calc-box__row-label">&nbsp;</span>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">сфера</span>
                            <div class="calc-box__select">
                                <select data-jcf='{"wrapNative": false}' class="diopter">
                                    <option>- 12.00</option>
                                    <option>- 1.00</option>
                                    <option>+ 12.00</option>
                                </select>
                            </div>
                        </div>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">упаковок</span>
                            <div class="calc-box__amount">
                                <a href="#" class="minus"></a>
                                <a href="#" class="plus"></a>
                                <input type="text" class="text" value="3">
                            </div>
                        </div>
                        <div class="calc-box__price-col">
                            <span class="calc-box__price">870 <span class="price-value">i</span></span>
                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                        </div>
                    </div>
                </div>
                <div class="calc-box__tab2">
                    <div class="calc-box__row">
                        <span class="calc-box__row-label">Левый глаз</span>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">сфера</span>
                            <div class="calc-box__select">
                                <select data-jcf='{"wrapNative": false}' class="diopter">
                                    <option>- 12.00</option>
                                    <option>- 1.00</option>
                                    <option>+ 12.00</option>
                                </select>
                            </div>
                        </div>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">упаковок</span>
                            <div class="calc-box__amount">
                                <a href="#" class="minus"></a>
                                <a href="#" class="plus"></a>
                                <input type="text" class="text" value="3">
                            </div>
                        </div>
                        <div class="calc-box__price-col">
                            <span class="calc-box__price">870 <span class="price-value">i</span></span>
                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                        </div>
                    </div>
                    <div class="calc-box__row">
                        <span class="calc-box__row-label">правый глаз</span>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">сфера</span>
                            <div class="calc-box__select">
                                <select data-jcf='{"wrapNative": false}' class="diopter">
                                    <option>- 12.00</option>
                                    <option>- 1.00</option>
                                    <option>+ 12.00</option>
                                </select>
                            </div>
                        </div>
                        <div class="calc-box__row-col">
                            <span class="calc-box__col-label">упаковок</span>
                            <div class="calc-box__amount">
                                <a href="#" class="minus"></a>
                                <a href="#" class="plus"></a>
                                <input type="text" class="text" value="3">
                            </div>
                        </div>
                        <div class="calc-box__price-col">
                            <span class="calc-box__price">870 <span class="price-value">i</span></span>
                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                        </div>
                    </div>
                </div>

                <div class="calc-box__total">
                    <a href="#" class="calc-box__compare tooltip-parent"><span class="tooltip">Добавить в сравнение</span></a>
                    <span class="calc-box__total-sum">1 870 <span class="price-value">i</span></span>
                </div>
                <div class="h_center">
                    <div class="btns__footer">
                        <a href="#" class="btn-blue btn-large"><span>купить</span></a>
                        <a href="#" class="btn-yellow btn-large"><span>купить в 1 клик</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item-promo-box animatedParent animateOnce bg-image">
    <img src="images/img12.jpg" alt="">
    <div class="container animated fadeInUpShort">
        <h2>15% скидка</h2>
        <h3>на линзы ACUVUE до 21 июня</h3>
        <div class="h_center">
            <a href="#" class="btn-blue btn-large"><span>подробнее</span></a>
        </div>
        <div class="animated-title animated">
            <a href="#" class="animated-title__text">все&nbsp;<span class="mark-blue">акции</span></a>
            <div class="corner-box"><div></div></div>
        </div>
    </div>
</div>
<div class="char-slider-wrap" id="char-section">
    <h3>характеристики</h3>
    <div class="animated-circle hidden-md-down">
        <div id="circle-1"></div>
        <div id="circle-2"></div>
        <div id="circle-3"></div>
    </div>
    <div class="char-slider hidden-md-down">
        <div>
            <div class="char-col">
                <div class="holder holder_left">
                    <div class="char-box char-box01">
                        <span class="char-box__title">тип материала</span>
                        <span class="char-box__sub-title">Силикон - гидрогель</span>
                    </div>
                    <div class="char-box char-box02">
                        <span class="char-box__title">Коэффициент пропускания кислорода, Dk/t</span>
                        <span class="char-box__sub-title">138</span>
                    </div>
                    <div class="char-box char-box03">
                        <span class="char-box__title">Влагосодержание, %</span>
                        <span class="char-box__sub-title">33 %</span>
                    </div>
                    <div class="char-box char-box04">
                        <span class="char-box__title">Диапозон рефракций</span>
                        <span class="char-box__sub-title">от - 10,0D до +6,0D</span>
                    </div>
                    <div class="char-box char-box05">
                        <span class="char-box__title">Радиус кривизны</span>
                        <span class="char-box__sub-title">8,6</span>
                    </div>
                </div>
            </div>
            <div class="char-col">
                <div class="holder holder_right">
                    <div class="char-box char-box01">
                        <span class="char-box__title">Срок замены</span>
                        <span class="char-box__sub-title">Один месяц</span>
                    </div>
                    <div class="char-box char-box02">
                        <span class="char-box__title">тип линз</span>
                        <span class="char-box__sub-title">Прозрачные</span>
                    </div>
                    <div class="char-box char-box03">
                        <span class="char-box__title">Производитель</span>
                        <span class="char-box__sub-title">Alcon</span>
                    </div>
                    <div class="char-box char-box04">
                        <span class="char-box__title">Количество блистеров</span>
                        <span class="char-box__sub-title">В упаковке 3 штуки</span>
                    </div>
                    <div class="char-box char-box05">
                        <span class="char-box__title">Режим ношения</span>
                        <span class="char-box__sub-title">Дневной</span>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="char-col">
                <div class="holder holder_left">
                    <div class="char-box char-box01">
                        <span class="char-box__title">Количество блистеров</span>
                        <span class="char-box__sub-title">В упаковке 3 штуки</span>
                    </div>
                    <div class="char-box char-box02">
                        <span class="char-box__title">Производитель</span>
                        <span class="char-box__sub-title">Alcon</span>
                    </div>
                    <div class="char-box char-box03">
                        <span class="char-box__title">тип линз</span>
                        <span class="char-box__sub-title">Прозрачные</span>
                    </div>
                    <div class="char-box char-box04">
                        <span class="char-box__title">Срок замены</span>
                        <span class="char-box__sub-title">Один месяц</span>
                    </div>
                    <div class="char-box char-box05">
                        <span class="char-box__title">Режим ношения</span>
                        <span class="char-box__sub-title">Дневной</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="char-slider-mobile hidden-lg-up">
        <div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
        </div>
        <div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
        </div>
        <div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
            <div class="char-box">
                <span class="char-box__title">тип материала</span>
                <span class="char-box__sub-title">Силикон - гидрогель</span>
            </div>
        </div>
    </div>
</div>
<div class="item-description">
    <h3>Описание Acuvue Oasys with Hydraclear Plus</h3>
    <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>
    <p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>
    <div class="slide-holder">
        <div class="slide">
            <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>
            <p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>
        </div>
        <div class="h_center">
            <a href="#" class="slide-holder__opener show-more"><span>показать <b>еще</b></span><em>скрыть</em><div class="corner-box"><div></div></div></a>
        </div>
    </div>
</div>







<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=$component->getSiteId()?>'
	});

	var jcc<?//=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);