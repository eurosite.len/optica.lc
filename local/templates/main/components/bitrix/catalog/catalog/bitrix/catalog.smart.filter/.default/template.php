<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
    'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME']))
{
    $this->addExternalCss($templateData['TEMPLATE_THEME']);
}
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>


<!--<aside class="aside animated fadeInUpShort">-->
<!--    <form action="#">-->
<!--        <ul class="filter-accordion">-->
<!--            <li class="active">-->
<!--                <a href="#" class="opener">По типу</a>-->
<!--                <div class="slide">-->
<!--                    <ul class="filter-list">-->
<!--                        <li>-->
<!--                            <input type="checkbox" class="checkbox" checked="checked" id="id200">-->
<!--                            <label for="id200">-->
<!--                                <span class="text">Однодневные линзы</span>-->
<!--                                <span class="amount">15</span>-->
<!--                            </label>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="#" class="opener">По бренду</a>-->
<!--                <div class="slide">-->
<!--                    <ul class="filter-list">-->
<!--                        <li>-->
<!--                            <a href="#">-->
<!--                                <span class="text">Однодневные линзы</span>-->
<!--                                <span class="amount">15</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </li>-->
<!---->
<!---->
<!--        </ul>-->
<!--        <div class="btns__footer">-->
<!--            <button class="btn-blue btn-large"><span>Подобрать</span></button>-->
<!--            <button class="btn-yellow btn-large"><span>Сбросить</span></button>-->
<!--        </div>-->
<!--    </form>-->





















<aside class="aside animated fadeInUpShort">
    <div class="bx-filter <?=$templateData["TEMPLATE_CLASS"]?>">

        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
        <?foreach($arResult["HIDDEN"] as $arItem):?>
            <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
        <?endforeach;?>
        <ul class="filter-accordion">
            <?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
            {
                $key = $arItem["ENCODED_ID"];
                if(isset($arItem["PRICE"])):
                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                        continue;

                    $step_num = 4;
                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                    $prices = array();
                    if (Bitrix\Main\Loader::includeModule("currency"))
                    {
                        for ($i = 0; $i < $step_num; $i++)
                        {
                            $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                        }
                        $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                    }
                    else
                    {
                        $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                        for ($i = 0; $i < $step_num; $i++)
                        {
                            $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
                        }
                        $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                    }
                    ?>
                    <div style="display: none;" class="<?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"):?>col-sm-6 col-md-4<?else:?>col-lg-12<?endif?> bx-filter-parameters-box bx-active">
                        <span class="bx-filter-container-modef"></span>
                        <div class="bx-filter-parameters-box-title" onclick="smartFilter.hideFilterProps(this)"><span><?=$arItem["NAME"]?> <i data-role="prop_angle" class="fa fa-angle-<?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>up<?else:?>down<?endif?>"></i></span></div>
                        <div class="bx-filter-block" data-role="bx_filter_block">
                            <div class="row bx-filter-parameters-box-container">
                                <div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
                                    <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
                                    <div class="bx-filter-input-container">
                                        <input
                                                class="min-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
                                    <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
                                    <div class="bx-filter-input-container">
                                        <input
                                                class="max-price"
                                                type="text"
                                                name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                                size="5"
                                                onkeyup="smartFilter.keyup(this)"
                                        />
                                    </div>
                                </div>

                                <div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
                                    <div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
                                        <?for($i = 0; $i <= $step_num; $i++):?>
                                            <div class="bx-ui-slider-part p<?=$i+1?>"><span><?=$prices[$i]?></span></div>
                                        <?endfor;?>

                                        <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                                        <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                                        <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                                        <div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
                                            <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
                                            <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?
                $arJsParams = array(
                    "leftSlider" => 'left_slider_'.$key,
                    "rightSlider" => 'right_slider_'.$key,
                    "tracker" => "drag_tracker_".$key,
                    "trackerWrap" => "drag_track_".$key,
                    "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                    "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                    "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                    "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                    "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                    "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                    "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                    "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                    "precision" => $precision,
                    "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                    "colorAvailableActive" => 'colorAvailableActive_'.$key,
                    "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                );
                ?>
                    <script type="text/javascript">
                        BX.ready(function(){
                            window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                        });
                    </script>
                <?endif;
            }

            //not prices
            foreach($arResult["ITEMS"] as $key=>$arItem)
            {
                if(
                    empty($arItem["VALUES"])
                    || isset($arItem["PRICE"])
                )
                    continue;

                if (
                    $arItem["DISPLAY_TYPE"] == "A"
                    && (
                        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                    )
                )
                    continue;
                ?>
                <div style="display: none;" class="<?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"):?>col-sm-6 col-md-4<?else:?>col-lg-12<?endif?> bx-filter-parameters-box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>bx-active<?endif?>">
                    <span class="bx-filter-container-modef"></span>

                    <div class="bx-filter-block" data-role="bx_filter_block">
                        <div class="row bx-filter-parameters-box-container">
                            <?
                            $arCur = current($arItem["VALUES"]);
                            switch ($arItem["DISPLAY_TYPE"])
                            {

                                default://CHECKBOXES
                                    ?>
                                    <div class="col-xs-12">
                                        <?foreach($arItem["VALUES"] as $val => $ar):?>
                                            <div class="checkbox">
                                                <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
													<span class="bx-filter-input-checkbox">
														<input
                                                                type="checkbox"
                                                                value="<? echo $ar["HTML_VALUE"] ?>"
                                                                name="<? echo $ar["CONTROL_NAME"] ?>"
                                                                id="<? echo $ar["CONTROL_ID"] ?>"
															<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
															onclick="smartFilter.click(this)"
                                                        />
														<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
                                                            if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                                ?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                                            endif;?></span>
													</span>
                                                </label>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>









            <li class=" <? if ($arItem["DISPLAY_EXPANDED"] == "Y"):?>active<? endif ?>">
                <a href="#" class="opener"><?= $arItem["NAME"] ?></a>
                <div class="slide" data-role="bx_filter_block">
                    <ul class="filter-list">
                        <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                        <li>
                            <input
                                    type="checkbox"
                                    class="checkbox"
                                    id="id200<? echo $ar["CONTROL_NAME"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                            />
                            <label for="id200<? echo $ar["CONTROL_NAME"] ?>">
                                    <span class="text"
                                          onclick="$('input#<?= $ar["CONTROL_ID"] ?>').click()"><? echo $ar["VALUE"] ?></span>

                            </label>

                        <li>
                            <? endforeach;?>
                    </ul>

                </div>
            </li>
                <?
            }
            ?>
        </ul>
        <div class="btns__footer">
            <input
                    class="btn-blue btn-large"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value="Подобрать"
            />
            <input
                    class="btn-yellow btn-large"
                    type="submit"
                    id="del_filter"
                    name="del_filter"
                    value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
            />
            <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
                <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
                <span class="arrow"></span>
                <br/>
                <a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
            </div>
        </div>
    </form>
    </div>
</aside>

<!--number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "")-->
<!--number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "")-->

<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>