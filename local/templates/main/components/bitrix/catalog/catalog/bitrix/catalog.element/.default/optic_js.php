
<script>
    ///?action=ADD2BASKET&id=154&quantity=5
    var prop=<?=json_encode($arResult['FILTERED']["ARRAY_JS"],JSON_UNESCAPED_UNICODE);?>;
var opt_href={one:{command:[],val:null},left:{command:[],val:null},right:{command:[],val:null}};
var opt_compare={one:[],left:[],right:[]};

var mbase_url="/ajax/add_to_basket.php";
var flagSendMess;
var product_id="<?=$arResult['ID']?>";
//
        $(".page-nav").addClass("page-nav_social animated fadeInUpShort");
        //Вставляяем социальные иконки из области
$(".page-nav").append('<?
    $file=file_get_contents(\Bitrix\Main\Application::getDocumentRoot().SITE_TEMPLATE_PATH."/include_areas/detail_social.php");
    if(!empty($file)){echo str_replace(array("\r\n", "\n", "\r"),'',$file); }
    ?>');

//Возвращает массив доступных предложений
function indexExist(description,value,array_src)
{
    var arrRes=array_src.filter(function(item){
        var len=item.DESCRIPTION.length;
        for(var i=0;i<len;i++){
            if((item.DESCRIPTION[i]==description)&&(item.VALUE[i]==value)){
                return true;
            }
        }
        return (false);
    });
    return arrRes;
}
//
function getVarId(description,value,result) {
    for (var i = 0; i < description.length; i++)
    {
        result = indexExist(description[i], value[i], result).slice();
    }
    return result;
}

//
function getId(description,value,result) {
    var resultOnce=[];
    var resultOncey = getVarId(description,value,result).slice();
    resultOnce=resultOncey.slice();
    for (var i = 0; i < resultOnce.length; i++)
    {
        if (description.length != resultOnce[i].DESCRIPTION.length)
        {
            resultOnce.splice(i, 1);
            --i;
        }
    }
    if(resultOnce.length==0)
    {resultOnce=resultOncey.slice();}
    return resultOnce;
}

//
function addToArrForPost($tab)
{
    var addDescr=[],addVal=[],count;
    addDescr.push($('.item-info__contacts .catalog-item__amount').attr('title'));
    addVal.push($('.item-info__contacts .catalog-item__amount .active').html());
    $('select',$tab).each(function () {
        if($(this).val()!=' ')
        {addVal.push($(this).val());
            addDescr.push($(this).attr('data-title'));}
    });
    count=$('input',$tab).val();

    var attrToPost=[];
    return getId(addDescr,addVal,prop);
}

    var flag_state_change=false;
    $('.calc-box__amount input').on('change',function(){
        var tab=$(this).parents('.calc-box__row').first();
        var value=null,result=null;
        var price=null;
        price=$('.calc-box__price',tab).attr('data-price');
        if(price!=='-'){value=(+$(this).val())*(+price);}else{value='-';}
        $('.calc-box__price___val',tab).html(value);
        if(!flag_state_change){
            var obj1=$('.calc-box__tab2 .calc-box__price___val').first().html();
            var obj2=$('.calc-box__tab2 .calc-box__price___val').last().html();
            if((obj1==='-')||(obj2==='-')){result='-';}else{result=(+obj1)+(+obj2);}
        }else{
            var obj=$('.calc-box__tab1 .calc-box__price___val').html();
            if(obj==='-'){result='-';}else{result=+obj;}
        }
        $('.calc-box__total-sum___val').html(result);

    });

    var first_three_count=0;

    function firstThree(){
        first_three_count++;
        if(first_three_count<4){return true;}else{return false;}
    }

//
$('.calc-box__tab1 select,.calc-box__tab2 select').on('change',function(){
    var arrParam=[];
    var tab=$(this).parents('.calc-box__row').first();
    var arrId=[];
    var arrId=addToArrForPost(tab);
    console.log(arrId);
    $('.calc_box__count',tab).html(arrId.length).attr("data-count",arrId.length);
    flagSendMess=false;
    if(!$(tab).hasClass('one_line')){
        //два комплекта
        if(($(".calc_box__count.left_count").attr("data-count")=="1") &&
            ($(".calc_box__count.right_count").attr("data-count")=="1"))
        {flagSendMess=true;}

    }else{
        //один комплект
        if(arrId.length==1){flagSendMess=true;}
    }
    var command=[],commandCompare=[], elemVal=null, id_offer=null,value='-';
    if(arrId.length==1){id_offer=arrId[0].ID_OFFERS;elemVal=arrId[0];value=elemVal.OFFERS_MIN_PRICE;}

    command.push({name: "action", value: "ADD2BASKET"});
    command.push({name: "id", value: id_offer });
    command.push({name: "quantity", value: $(".calc-box__amount input",tab).val()});
    command.push({name: "id_product", value: product_id});
    commandCompare.push({name:'action',value:'ADD_TO_COMPARE_LIST'});
    commandCompare.push({name:'id',value:id_offer});
    //?action=ADD_TO_COMPARE_LIST&id=96

    if($(tab).hasClass('one_line')){opt_href.one.command=command;
                                    opt_compare.one=commandCompare;
                                    opt_href.one.val=elemVal;}
    if($(tab).hasClass('left_line')){opt_href.left.command=command;opt_compare.left=commandCompare;opt_href.left.val=elemVal;}
    if($(tab).hasClass('right_line')){opt_href.right.command=command;opt_compare.right=commandCompare;opt_href.right.val=elemVal;}
    $('.calc-box__price',tab).attr('data-price',value);

    var result=null;
    var price=null;
    if(value!=='-'){price=(+$('input',tab).val())*(+value);}else{price='-';}
    $('.calc-box__price___val',tab).html(price);
    var three=firstThree();
    console.log(three);
    if(!flag_state_change||three){

        var obj1=$('.calc-box__tab2 .calc-box__price___val').first().html();
        var obj2=$('.calc-box__tab2 .calc-box__price___val').last().html();
        console.log('obj1='+obj1);
        console.log('obj2='+obj2);
        if((obj1==='-')||(obj2==='-')){result='-';}else{result=(+obj1)+(+obj2);}
    }else{
        var obj=$('.calc-box__tab1 .calc-box__price___val').html();
        console.log('obj='+obj);
        if(obj==='-'){result='-';}else{result=+obj;}
    }
    console.log('result='+result);
    $('.calc-box__total-sum___val').html(result);
});
    //
$('.catalog-item__amount').on('click','catalog-item__amount a',function(){
    $('#select_0_one,#select_0_two_left,#select_0_two_right').trigger('change');
    });
//
$('label[for="checkbox02"],label[for="checkbox03"]').on('click',function(){
    if($(this).attr("for")=='checkbox02'){flag_state_change=true;}
    if($(this).attr("for")=='checkbox03'){flag_state_change=false;}
    if(!flag_state_change)
    {
        $('#select_0_two_left,#select_0_two_right').trigger('change');

    }
    else{
        $('#select_0_one').trigger('change');
    };}

);
//
$('#select_0_one,#select_0_two_left,#select_0_two_right').trigger('change');

$('.calc-box .btns__footer .btn-buy').on('click',function(){
        if(!flagSendMess)return;
        if(!flag_state_change) {
            $.post( mbase_url ,opt_href.left.command)
                .done(function(result){ $('.modal-main').css('display','block');})
                .fail(function(result){alert("Ошибка сети");});
            $.post(mbase_url ,opt_href.right.command)
                .done(function(result){ $('.modal-main').css('display','block');})
                .fail(function(result){alert("Ошибка сети");});
        }
        else{
            $.post( mbase_url ,opt_href.one.command)
                .done(function(result){ $('.modal-main').css('display','block');})
                .fail(function(result){alert("Ошибка сети");});
        }
    }
);
//
$('.calc-box .compare').on('click',function(){
    if(!flagSendMess)return;
    if(!flag_state_change) {
        $.post( '/catalog/' ,opt_compare.left)
            .done(function(result){})
            .fail(function(result){alert("Ошибка сети");});
        $.post('/catalog/' ,opt_compare.right)
            .done(function(result){ })
            .fail(function(result){alert("Ошибка сети");});
    }
    else{$.post( '/catalog/' ,opt_compare.one)
            .done(function(result){})
            .fail(function(result){alert("Ошибка сети");});
    }
});
//
</script>