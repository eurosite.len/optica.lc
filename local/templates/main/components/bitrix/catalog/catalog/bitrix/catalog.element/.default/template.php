<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */



$this->setFrameMode(true);
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jcf.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/mag.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.zoom.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/zoom-init.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/cart-calc.js");

$strMainID = $this->GetEditAreaId($arResult['ID']);

$countVal=array_shift($arResult['FILTERED']['VALUE']);
array_shift($countVal);
$countDescr=array_shift($arResult['FILTERED']['DESCRIPTION']);

//var_dump($arResult);
?>

                </div>
<? include  \Bitrix\Main\Application::getDocumentRoot().$templateFolder."/optic_js.php"?>

                <div class="item-info animatedParent animateOnce">
                    <svg version="1.1" id="triangle" class="animated figure1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  width="124" height="160" viewBox="0 0 124.007 108.001" style="enable-background:new 0 0 124.007 108.001;" xml:space="preserve">
                        <g>
                            <g>
                                <path class="path" transform="rotate(-180, 60.0194, 60.3266)" id="svg_5" d="m2.966791,110.247635l57.052565,-99.841972l57.052567,99.841972z" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff"/>
                            </g>
                        </g>
                    </svg>
                    <svg version="1.1" id="dots" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="31" height="48" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                        <g>
                            <g>
                                <path class="dot01 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                            </g>
                        </g>
                    </svg>
                    <svg version="1.1" id="dots2" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="150" height="150" style="enable-background:new 0 0 30.346 47.516;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="dot01 dot dot-l" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot02 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot03 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot04 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot05 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot06 dot" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot07 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot08 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot09 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot10 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot11 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                    <path class="dot12 dot dot-r" fill="rgb(0,0,0)" fill-opacity="1" d="M0 0 M-4,0 C-4,2.21 -2.209,4 0.001,4 C2.209,4 4,2.21 4,0 C4,-2.209 2.209,-4 0.001,-4 C-2.209,-4 -4,-2.209 -4,0z"></path>
                                </g>
                            </g>
                    </svg>
                    <svg id="lines" class="animated" xmlns="http://www.w3.org/2000/svg" width="100" height="36" viewBox="0 0 95.85 31.99">
                        <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,5.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,8.72,1.26,11-8.56,20.88-8.5C58.5,2.78,60.86,13,69.62,11.85c7.61-1,9.85-9.32,18-9.25A14.78,14.78,0,0,1,96,5.6"/>
                        <path class="path2" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,14.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,22,69.62,20.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
                        <path class="path3" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,24.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,32,69.62,30.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
                    </svg>
                    <svg version="1.1" id="vertical-line" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 120" style="enable-background:new 0 0 18 120;" xml:space="preserve" width="20" height="122">
                        <g>
                            <g>
                                <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M17.218,30.663c0.602-0.757,1.204-1.589,0.377-2.27
                                    l-7.14-6.058c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272L6.1,0H1.741l6.085,5.149
                                    c0.827,0.68,0.152,1.663-0.374,2.27l-6.988,8.027c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                    c0.751,0.606,0.152,1.666-0.374,2.196L0.464,33.92c-0.753,0.909-0.526,1.59,0.225,2.196l7.137,6.058
                                    c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119l7.365,6.396
                                    c0.335,0.64-0.159,1.41-0.602,1.922l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.12l7.137,6.132
                                    c0.751,0.606,0.152,1.666-0.374,2.196L0.464,89.41c-0.753,0.91-0.526,1.59,0.225,2.196l7.137,6.058
                                    c0.827,0.68,0.152,1.666-0.374,2.27l-6.988,8.028c-0.753,0.833-0.452,1.513,0.225,2.119L12.109,120h4.283l-6.085-5.299
                                    c-0.751-0.606-0.976-1.287-0.225-2.196l6.988-8.025c0.602-0.683,1.352-1.363,0.525-2.046l-7.14-6.056
                                    c-0.751-0.606-1.125-1.44-0.225-2.196l6.988-8.027c0.602-0.757,1.204-1.589,0.377-2.27l-7.14-6.058
                                    c-0.751-0.606-1.125-1.439-0.225-2.196l6.988-8.025c0.602-0.682,1.204-1.516,0.377-2.272l-7.77-6.653
                                    c-0.306-0.475-0.284-1.01,0.256-1.666l6.988-8.026c0.602-0.682,1.352-1.363,0.525-2.046l-7.14-6.056
                                    c-0.751-0.606-1.125-1.439-0.225-2.196L17.218,30.663z"/>
                            </g>
                        </g>
                    </svg>

                    <svg version="1.1" id="lines3" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 410 46" style="enable-background:new 0 0 18 120;" xml:space="preserve" width="410" height="46">
                        <g>
                            <g>
                               <circle class="pie1" cx="0" cy="0" r="4"/>
                               <path class="line1" fill="rgb(0,0,0)" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff" d="M0,1H370"></path>
                               <path class="line2" fill="rgb(0,0,0)" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000" fill="#fff" d="M0,1H370"></path>
                            </g>
                        </g>
                    </svg>
                    <?/*<a href="#" class="gift-link hidden-md-down animated fadeInUpShort tooltip-parent"><span class="tooltip">к этому товару предлагается подарок</span></a>*/?>
                    <div class="output_mess"></div>
                    <div class="row no-gutters animated fadeInUpShort">
                        <div class="col-12 col-md-5 item-info__contacts">
                            <span class="item-info__presence">Товар доступен только в нашем магазине</span>
                            <span class="item-info__article">Артикул: <?=$arResult['OFFERS'][0]['PROPERTIES']['CML2_ARTICLE']['~VALUE']?><span></span>
                            <span class="item-info__label">Количество линз в упаковке</span>
                            <ul class="catalog-item__amount" title="<?=$countDescr?>">

                                <?foreach($countVal as $key=>$item):?>
                                    <li><a href="#" class="<?if($key==0) echo 'active';?>"><?=$item?></a></li>
                                <?endforeach;?>
                            </ul>
                           <?$APPLICATION->IncludeComponent(
                               "bitrix:main.include",
                               "",
                               Array(
                                   "AREA_FILE_SHOW" => "file",
                                   "PATH" => SITE_TEMPLATE_PATH."/include_areas/detail_phone.php"
                               ),$component
                           );?>
                            <div class="view-all-chars">
                                <a href="#char-section" class="anchor-link btn-blue"><span>Все характеристики</span></a>

                            </div>
                        </div>
                        <div class="col-12 col-md item-info__slider-wrap">
                            <a href="#" class="gift-link hidden-lg-up"></a>
                            <div class="items-slider">
                                <?$arr_size=count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']);
                                    for($i=0;$i<$arr_size;$i++):?>
                                <div>
                                    <div class="small zoom-item">
                                        <img src="<?=CFile::GetPath($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'][$i])?>"
                                             alt="<?=$arResult['PROPERTIES']['MORE_PHOTO']['DESCRIPTION'][$i]?>">
                                    </div>
                                </div>
                                <?endfor?>
                            </div>
                            <div class="items-slider-nav">
                                <?$arr_size=count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']);
                                for($i=0;$i<$arr_size;$i++):?>
                                <div>
                                        <div class="holder">
                                            <img src="<?=CFile::GetPath($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'][$i])?>"
                                                 alt="<?=$arResult['PROPERTIES']['MORE_PHOTO']['DESCRIPTION'][$i]?>">
                                            <div class="corner-box"><div></div></div>
                                        </div>
                                </div>
                                <?endfor?>
                            </div>
                        </div>
                        <div class="col-12 col-lg item-info__calc">
                            <div class="calc-box">
                                <div class="corner-box"><div></div></div>
                                <ul class="calc-box__check-list">
                                    <li>
                                        <div class="check-wrap check-wrap_type">
                                            <input type="radio" class="check" id="checkbox02" name="1">
                                            <label for="checkbox02">одинаковые</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="check-wrap check-wrap_type">
                                            <input type="radio" class="check" checked="checked" id="checkbox03" name="1">
                                            <label for="checkbox03">различающиеся</label>
                                        </div>
                                    </li>
                                </ul>
                                <div class="calc-box__tab1">
                                    <div class="calc-box__row one_line">
                                        <span class="calc-box__row-label">&nbsp;</span>

                                        <?foreach($arResult['FILTERED']["DESCRIPTION"] as $key=>$itemDescription):?>
                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label"><?=$itemDescription?></span>
                                            <div class="calc-box__select">
                                                <select data-jcf='{"wrapNative": false}' class="diopter" id="select_<?=$key?>_one" data-title="<?=$itemDescription?>">
                                                    <?foreach($arResult['FILTERED']["VALUE"][$key] as $item):?>
                                                        <option value="<?=$item?>"><?=$item?></option>
                                                    <?endforeach;?>
                                                </select>

                                            </div>
                                        </div>
                                        <?endforeach;?>
                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label">упаковок</span>
                                            <div class="calc-box__amount">
                                                <a href="#" class="minus"></a>
                                                <a href="#" class="plus"></a>
                                                <input type="text" class="text" value="3">
                                            </div>
                                        </div>

                                        <div class="calc-box__price-col">
                                            <span class="calc-box__price-note calc_box__count" data-count=""></span>
                                            <span class="calc-box__price" data-price="-"><span class="calc-box__price___val">-</span> <span class="price-value">i</span></span>
                                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="calc-box__tab2">
                                    <div class="calc-box__row left_line">
                                        <span class="calc-box__row-label ">Левый глаз</span>

                                        <?foreach($arResult['FILTERED']["DESCRIPTION"] as $key=>$itemDescription):?>
                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label"><?=$itemDescription?></span>
                                            <div class="calc-box__select">
                                                <select data-jcf='{"wrapNative": false}' class="diopter" id="select_<?=$key?>_two_left" data-title="<?=$itemDescription?>">
                                                    <?foreach($arResult['FILTERED']["VALUE"][$key] as $item):?>
                                                        <option value="<?=$item?>"><?=$item?></option>
                                                    <?endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <?endforeach;?>

                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label">упаковок </span>
                                            <div class="calc-box__amount">
                                                <a href="#" class="minus"></a>
                                                <a href="#" class="plus"></a>
                                                <input type="text" class="text quantity" value="3">
                                            </div>
                                        </div>
                                        <div class="calc-box__price-col">
                                            <span class="calc-box__price-note calc_box__count left_count" data-count=""></span>
                                            <span class="calc-box__price" data-price="-"><span class="calc-box__price___val">-</span> <span class="price-value">i</span></span>
                                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                                        </div>
                                    </div>
                                    <div class="calc-box__row right_line">
                                        <span class="calc-box__row-label">правый глаз</span>

                                        <?foreach($arResult['FILTERED']["DESCRIPTION"] as $key=>$itemDescription):?>
                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label"><?=$itemDescription?></span>
                                            <div class="calc-box__select">
                                                <select data-jcf='{"wrapNative": false}' class="diopter" id="select_<?=$key?>_two_right" data-title="<?=$itemDescription?>">
                                                    <?foreach($arResult['FILTERED']["VALUE"][$key] as $item):?>
                                                        <option value="<?=$item?>"><?=$item?></option>
                                                    <?endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <?endforeach;?>

                                        <div class="calc-box__row-col">
                                            <span class="calc-box__col-label">упаковок</span>
                                            <div class="calc-box__amount">
                                                <a href="#" class="minus"></a>
                                                <a href="#" class="plus"></a>
                                                <input type="text" class="text quantity" value="3">
                                            </div>
                                        </div>
                                        <div class="calc-box__price-col">
                                            <span class="calc-box__price-note calc_box__count right_count" data-count=""></span>
                                            <span class="calc-box__price" data-price="-"><span class="calc-box__price___val">-</span> <span class="price-value">i</span></span>
                                            <span class="calc-box__price-note">114 <span class="price-value">i</span> в день </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="calc-box__total">

                                    <a class="compare" rel="nofollow" style="cursor:pointer;">добавить к сравнению</a>

                                    <span class="calc-box__total-sum"><span class="calc-box__total-sum___val">-</span> <span class="price-value">i</span></span>
                                </div>
                                <div class="h_center">
                                    <div class="btns__footer">
                                        <a style="cursor:pointer;" class="btn-blue btn-large btn-buy"><span>купить</span></a>
                                        <a style="cursor:pointer;" class="btn-yellow btn-large btn-one"><span>купить в 1 клик</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-promo-box animatedParent animateOnce bg-image">
                    <img src="images/img12.jpg" alt="">
                    <div class="container animated fadeInUpShort">
                        <h2>15% скидка</h2>
                        <h3>на линзы ACUVUE до 21 июня</h3>
                        <div class="h_center">
                            <a href="#" class="btn-blue btn-large"><span>подробнее</span></a>
                        </div>
                        <div class="animated-title animated">
                            <a href="#" class="animated-title__text">все&nbsp;<span class="mark-blue">акции</span></a>
                            <div class="corner-box"><div></div></div>
                        </div>
                    </div>
                </div>
                <div class="char-slider-wrap" id="char-section">
                    <h3>характеристики</h3>
                    <div class="animated-circle hidden-md-down">
                        <div id="circle-1"></div>
                        <div id="circle-2"></div>
                        <div id="circle-3"></div>
                    </div>
                    <div class="char-slider hidden-md-down">
                        <div>
                            <div class="char-col">
                                <div class="holder holder_left">
                                    <div class="char-box char-box01">
                                        <span class="char-box__title">Тип материала</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['TIP_MATERIALA']['~VALUE']?></span>
                                    </div>
                                    <div class="char-box char-box02">
                                        <span class="char-box__title">Коэффициент пропускания кислорода, Dk/t</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['KISLORODOPRONITSAEMOST']['~VALUE']?></span>
                                    </div>
                                    <div class="char-box char-box03">
                                        <span class="char-box__title">Влагосодержание, %</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['VLAGOSODERZHANIE']['~VALUE']?> %</span>
                                    </div>
                                    <div class="char-box char-box04">
                                        <span class="char-box__title">Базовый радиус кривизны</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['BAZOVAYA_KRIVIZNA']['~VALUE']?> </span>
                                           </div>
                                    <div class="char-box char-box05">
                                        <span class="char-box__title">Вариантов кривизны</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['VARIANTY_KRIVIZNY']['~VALUE']?> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="char-col">
                                <div class="holder holder_right">
                                    <div class="char-box char-box01">
                                        <span class="char-box__title">Срок замены</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['MODALNOST']['~VALUE']?></span>
                                    </div>
                                    <div class="char-box char-box02">
                                        <span class="char-box__title">Тип линз</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['TIP_LINZ']['~VALUE']?></span>
                                    </div>
                                    <div class="char-box char-box03">
                                        <span class="char-box__title">Производитель</span>
                                        <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['PROIZVODITEL']['~VALUE']?></span>
                                    </div>
                                    <div class="char-box char-box04">

                                    </div>
                                    <div class="char-box char-box05">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="char-slider-mobile hidden-lg-up">
                        <div>
                            <div class="char-box">
                                <span class="char-box__title">Тип материала</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['TIP_MATERIALA']['~VALUE']?></span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Коэффициент пропускания кислорода, Dk/t</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['KISLORODOPRONITSAEMOST']['~VALUE']?></span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Влагосодержание, %</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['VLAGOSODERZHANIE']['~VALUE']?> %</span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Диапозон рефракций</span>
                                <span class="char-box__sub-title">от <?=$arResult['FILTERED']['REFRACT']['MIN']?>D до <?=$arResult['FILTERED']['REFRACT']['MAX']?>D</span>
                            </div>
                        </div>
                        <div>
                            <div class="char-box">
                                <span class="char-box__title">Радиус кривизны</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['BAZOVAYA_KRIVIZNA']['~VALUE']?> </span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Срок замены</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['MODALNOST']['~VALUE']?></span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Тип линз</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['TIP_LINZ']['~VALUE']?></span>
                            </div>
                            <div class="char-box">
                                <span class="char-box__title">Базовый радиус кривизны</span>
                                <span class="char-box__sub-title"><?=$arResult['PROPERTIES']['BAZOVAYA_KRIVIZNA']['~VALUE']?> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-description">
                    <h3><?=$arResult['~NAME']?></h3>
                    <?$text = $arResult['~DETAIL_TEXT'];
                    //var_dump($arResult);
                    $paragraphsCount = 2;

                    $sentences = mb_split('\.', $text);
                    $paragraphs = array_chunk($sentences, ceil(count($sentences) / $paragraphsCount));

                    array_walk($paragraphs, function (&$paragraphSentences) {
                        $paragraphSentences = implode(' ', $paragraphSentences);
                    });?>
                     <p>
                     <?=$paragraphs[0];?>
                     </p>
                    <div class="slide-holder">
                        <div class="slide">
                        <p><?=$paragraphs[1];?>
                            <p>
                        </div>
                        <div class="h_center">
                            <a href="#" class="slide-holder__opener show-more"><span>показать <b>еще</b></span><em>скрыть</em><div class="corner-box"><div></div></div></a>
                        </div>
                    </div>
                </div>

<div class="modal-main buy-modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">×</span>
        </div>
        <div class="modal-body">
            <div>Товар добавлен</div>
            <div>в корзину</div>
        </div>
        <div class="modal-footer">
        </div>
    </div>

</div>

<div class="modal-main compare-modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">×</span>
        </div>
        <div class="modal-body">
            <div>Товар добавлен</div>
            <div>в сравнение</div>
        </div>
        <div class="modal-footer">
        </div>
    </div>

</div>
            </div>
        </div>



