<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="nav-popup"><?
    if (0 < $arResult["SECTIONS_COUNT"]) {
        ?>
        <nav class="nav">
            <ul>

                <?
                foreach ($arResult['SECTIONS'] as &$arSection) {
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                    ?>
                    <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><a
                            href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?

                    ?>
                    <div class="nav-drop">
                        <div class="nav-drop__col">
                            <span class="nav-drop__title">Категории</span>
                            <div class="nav-drop__image">
                                <a href="#"><img
                                            src="<?= SITE_TEMPLATE_PATH ?>/images/img04.jpg"
                                            alt="" width="208" height="119"></a>
                            </div>
                            <ul class="drop-list">
                                <li><a href="#">Капли</a></li>
                                <li><a href="#">Для жестких контактных линз</a></li>
                                <li><a href="#">Уценка, мятая упаковка.</a></li>
                            </ul>
                        </div>
                        <div class="nav-drop__col">
                            <span class="nav-drop__title">ПРОИЗВОДИТЕЛИ</span>
                            <div class="nav-drop__image">
                                <a href="#"><img
                                            src="<?= SITE_TEMPLATE_PATH ?>/images/img05.jpg"
                                            alt="" width="208" height="119"></a>
                            </div>
                            <ul class="drop-list">
                                <li><a href="#">Alcon/CIBA Vision</a></li>
                                <li><a href="#">Avizor</a></li>
                                <li><a href="#">Bausch&amp;Lomb</a></li>
                                <li><a href="#">Cliwell</a></li>
                                <li><a href="#">Maxima</a></li>
                                <li><a href="#">Ophthalmix</a></li>
                                <li><a href="#">Еще →</a></li>
                            </ul>
                        </div>
                    </div>
                    </li><?
                }

                ?>
            </ul>
        </nav>
        <?
        echo('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
    }
    ?></div>