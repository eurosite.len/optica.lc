function topicFilter(obj){
    var topic = $(obj).attr("data");
    $(".remove_active_filter_js").each(function(){
        $(this).removeClass("active");
    });
    $(obj).addClass("active");
    $.ajax({
        type: "POST",
        data: {CURRENT_TOPIC: topic, NEWS_FILTER_AJAX: "Y", PAGEN_1: 1},
        success: function(data){
            $(".js_deleted").remove();
            $("div.main.main_inner div.container").append(data);
            $('.bg-image').each(function() {
                if (typeof $(this).find('img').attr('src') != 'undefined') {
                    $(this).css('background-image', 'url(' + $(this).find('img').attr('src') + ')');
                }
            });
        }
    });
}