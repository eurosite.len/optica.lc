<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTopic = CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => 3), false, false, array("PROPERTY_TOPIC"));

$arElem = array();
$arSortElem = array();
$arSortElemFinal = array();
while($elem = $arTopic->fetch()){
    if(!in_array($elem["PROPERTY_TOPIC_VALUE"], $arElem)){
        $arElem[$elem["PROPERTY_TOPIC_ENUM_ID"]] = $elem["PROPERTY_TOPIC_VALUE"];
    }
    $db_props = CIBlockElement::GetProperty(3, $elem["ID"], array(), array("ID" => 57));
    while($props = $db_props->fetch()){
        if(!in_array($props["VALUE_ENUM"], $arSortElem)){
            $arSortElem[$props["VALUE_SORT"]] = $props["VALUE_ENUM"];
        }
    }
}

ksort($arSortElem);

foreach($arSortElem as $key => $val){
    if(array_search($val, $arElem)){
        $arSortElemFinal[$key]["ID"] = array_search($val, $arElem);
        $arSortElemFinal[$key]["TEXT"] = $arElem[array_search($val, $arElem)];
    }
}



$arResult["TOPICS_FILTER"] = $arSortElemFinal;
?>