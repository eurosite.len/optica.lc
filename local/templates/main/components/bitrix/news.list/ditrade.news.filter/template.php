<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>


<ul class="content-sort animated fadeInUpShort go js_deleted">
    <li>
        <a href="javascript:void(0);" class="remove_active_filter_js<?if(!isset($_REQUEST["CURRENT_TOPIC"]) || $_REQUEST["CURRENT_TOPIC"] == ""):?> active<?endif;?>" data="" onclick="topicFilter(this);">все
            <div class="corner-box corner-box_blue"><div></div></div>
        </a>
    </li>
    <?foreach($arResult["TOPICS_FILTER"] as $topic):?>
        <li>
        <a href="javascript:void(0);" class="remove_active_filter_js<?if($_REQUEST["CURRENT_TOPIC"] == $topic["ID"]):?> active<?endif;?>" data="<?=$topic["ID"];?>" onclick="topicFilter(this);"><?=$topic["TEXT"]?>
            <div class="corner-box corner-box_blue"><div></div></div>
        </a>
    </li>
    <?endforeach;?>
</ul>
