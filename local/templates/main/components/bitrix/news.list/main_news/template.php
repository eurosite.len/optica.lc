<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!empty($arResult["ITEMS"])): ?>
    <div class="row animated fadeInUpShort">
        <?
        foreach ($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="col-12 col-md-4" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="article-item bg-image bg-image_native bg-image_right-bottom">
                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['PREVIEW_PICTURE']['ALT'] ?>">
                    <div class="article-item__text-box">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                           class="article-item__title"><?= $arItem["NAME"] ?></a>
                        <p><?= $arItem["PREVIEW_TEXT"] ?></p>
                    </div>
                    <div class="article-item__theme">
                        <span class="article-item__label">В статье:</span>
                        <span class="article-item__note">
<? $list = '';
foreach ($arItem['DISPLAY_PROPERTIES']['TOPIC']['VALUE'] as $val) {

    $list .= "$val, ";
}
$list = trim($list, ', ');
echo $list
?></span>
                    </div>
                </div>
            </div>

        <? endforeach; ?>
    </div>
<? endif; ?>


