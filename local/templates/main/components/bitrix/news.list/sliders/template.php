<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (!empty($arResult["ITEMS"])): ?>
    <div class="main-slider animated fadeIn">
        <?
        foreach ($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="image-section bg-image">
                    <img src="<?= CFile::GetPath($arItem['PROPERTIES']['SLIDE']['VALUE']) ?>"
                         alt="<?= $arItem['PROPERTIES']['TITLE']['VALUE'] ?>">
                </div>
                <div class="text-box">
                    <h2><?= $arItem['PROPERTIES']['TITLE']['VALUE'] ?></h2>
                    <? if (!empty($arItem['PROPERTIES']['URL']['~VALUE'])):?>
                        <a href="<?= $arItem['PROPERTIES']['URL']['~VALUE'] ?>"
                           class="btn-full more"><span>Подробнее</span></a>
                    <? endif; ?>
                </div>
            </div>

        <? endforeach; ?>

    </div>
<? endif; ?>