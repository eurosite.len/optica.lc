<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();?>
<div class="social-holder bx-auth-serv-icons">
	<span class="social-holder__title">войти через</span>
	<ul class="social-list">
		<?foreach($arParams["~AUTH_SERVICES"] as $service):?>
			<?$icon=htmlspecialcharsbx($service["ICON"]);
			$name=$service["NAME"];
			if($service['ID']=='VKontakte'){
				$icon='vk';
				$name='vk';
			}if($service['ID']=='Facebook'){
				$icon='facebook';
				$name='facebook';
			}?>
		<li>
			<a  title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="<?=$service['ONCLICK']?>" class="<?=$icon?>"><?=$name?></a>
		</li>
		<?endforeach?>
	</ul>
</div>
