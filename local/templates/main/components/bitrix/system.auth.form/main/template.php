<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init();
?>
<?if($arResult["FORM_TYPE"] == "login"):?>
	<?if($arParams['SOCIAL_CERVIS_REG']!='Y'):?>
	<?if($_REQUEST["auth"]!="Y"):?>
	<form id='js_auth_form' name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?endif;?>
	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input class="js_auth_form" type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
		<input class="js_auth_form" type="hidden" name="AUTH_FORM" value="Y" />
		<input class="js_auth_form" type="hidden" name="TYPE" value="AUTH" />
	<?if($_REQUEST["auth"]=="Y" && $arResult['ERROR']):?>
		<div class="error-holder">
			<span class="error-text"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></span>
		</div>
	<?endif;?>
	<div class="text-field-wrap">
		<label for="form-email"><?=GetMessage("AUTH_LOGIN")?></label>
		<input type="text" name="USER_LOGIN" maxlength="50"  class="text-field js_auth_form" id="form-email">
	</div>
	<div class="text-field-wrap">
		<label for="form-pass"><?=GetMessage("AUTH_PASSWORD")?></label>
		<input type="password" name="USER_PASSWORD" maxlength="50" autocomplete="off" class="text-field text-field_last js_auth_form" id="form-pass" >
	</div>
	<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
	<div class="check-wrap h_spacer25">
		<input type="checkbox" class="check js_auth_form" id="checkbox01" name="USER_REMEMBER" value="Y" checked="checked">
		<label for="checkbox01" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?=GetMessage("AUTH_REMEMBER_SHORT")?></label>
	</div>
	<?endif?>
	<div class="submit-wrap">
		<button class="submit btn-blue" type="button" onclick="complit_auth_form();"><span><?=GetMessage("AUTH_LOGIN_BUTTON")?></span></button>
	</div>
	<hr>
	<div class="h_center">
		<a href="#forgot-popup" class="forgot" data-fancybox><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
	</div>
<?endif;?>
	<?
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "main",
	array(
	"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
	"SUFFIX"=>"form",
	),
	$component,
	array("HIDE_ICONS"=>"Y")
	);
	?>
	<?if($arParams['SOCIAL_CERVIS_REG']!='Y'):?>
	<?if($_REQUEST["auth"]!="Y"):?>
	</form>
	<?endif;?>
	<?endif;?>
<?else:?>
	js_reload_js
<?endif;?>