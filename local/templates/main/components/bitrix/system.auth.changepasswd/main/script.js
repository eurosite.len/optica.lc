function js_changepasswd(){
	var o = {};
	$('#js_changepasswd input.js_changepasswd').each(function () {
		o[$(this).attr('name')]=$(this).val();
	});
	o['forgotpasswd']='Y';
    jQuery(function ($) {
    $.ajax
		({
			type: "Post",
			url: "/ajax/changepasswd.php",
			data:o,
			dataType: "html",
			success: function(data)
			{
				if(data.indexOf('js_reload_js') + 1>0) {
					location.reload();
				}else{
					$('#js_changepasswd').html(data);
				}
			}
		});
    });
}