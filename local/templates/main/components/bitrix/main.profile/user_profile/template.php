<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<h2>Персональная информация</h2>
<div class="main-form">

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>

    <div>
        <?=$arResult["arUser"]["NAME"] . "<br>";?>
        <?=$arResult["arUser"]["LAST_NAME"] . "<br>";?>
        <?=$arResult["arUser"]["PERSONAL_PHONE"] . "<br>";?>

    </div>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

    <div class="static-row">
        <span class="static-row__label">e-mail:</span>
        <span class="static-row__title"><? echo $arResult["arUser"]["EMAIL"]?></span>
    </div>
    <div class="row static-row">
        <div class="col-12 col-md-7">
            <div class="text-field-wrap">
                <label for="form-name">Имя<sup>*</sup></label>
                <input type="text" name="NAME" class="text-field" id="form-name" maxlength="50" value="<?=$arResult["arUser"]["NAME"];?>" />
            </div>
        </div>
        <div class="col-12 col-md-5">
            <span class="row-note">Ваше имя необходимо для того, чтобы мы знали как к вам обращаться</span>
        </div>
    </div>
    <div class="row static-row">
        <div class="col-12 col-md-7">
            <div class="text-field-wrap">
                <label for="form-lastname">Фамилия<sup>*</sup></label>
                <input type="text" class="text-field" id="form-lastname" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"];?>" />
            </div>
        </div>
    </div>
    <div class="row static-row">
        <div class="col-12 col-md-7">
            <div class="text-field-wrap">
                <label for="form-phone">телефон<sup>*</sup></label>
                <input type="text" class="text-field phone-mask text-field_last" id="form-phone" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"];?>" />
            </div>
        </div>
        <div class="col-12 col-md-5">
            <span class="row-note">Контактный телефон нужен для уточнения деталей заказа</span>
        </div>
    </div>
    <div class="subscribe-check">
        <span class="subs-title">подписаться на рассылку</span>
        <ul class="check-list">
            <li>
                <div class="check-wrap">
                    <input type="checkbox" class="check" id="checkbox01">
                    <label for="checkbox01">смс</label>
                </div>
            </li>
            <li>
                <div class="check-wrap">
                    <input type="checkbox" class="check" id="checkbox02">
                    <label for="checkbox02">e-mail</label>
                </div>
            </li>
        </ul>
    </div>
    <input type="submit" class="btn-blue" name="save" value="сохранить изменения">
</form>
</div>
