<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
//var_dump($arResult);
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
    <a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="cart-link cart-link_opener">
        <span class="cart-link__items-amount"><?=$arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)']?></span>
        <span class="cart-link__items-sum"><?=$arResult['TOTAL_PRICE']?> <span class="price-value">i</span></span>
    </a>

