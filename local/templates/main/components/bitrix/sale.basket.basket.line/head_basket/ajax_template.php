<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)) . '/top_template.php');
if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY']))) {
    ?>
    <div class="cart__popup">
        <div id="<?= $cartId ?>products" class="cart__popup-content">
            <? foreach ($arResult["CATEGORIES"] as $category => $items):
                if (empty($items))
                    continue;
                foreach ($items as $v):?>
                <div class="cart__popup-row">
                    <div class="image">
                        <img src="<?= $v["DETAIL_PAGE_URL"] ?>" alt="<?= $v["NAME"] ?>">
                    </div>
                    <a href="<?= $v["DETAIL_PAGE_URL"] ?>" class="title"><?= $v["NAME"] ?></a>
                    <div class="amount-wrap">
                        <a href="#" class="minus">–</a>
                        <input type="text" class="text" value="1">
                        <a href="#" class="plus">+</a>
                    </div>
                    <a class="delete-item" onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"></a>
                    <span class="price"><span class="hider"><?= $v["SUM"] ?> </span> <span
                                class="price-value">i</span></span>
                </div>

            <? endforeach ?>
            <? endforeach ?>
        </div>
        <div class="cart__popup-footer">
            <div class="cart__popup-total">
                <span class="cart__popup__label">Итого</span>
                <span class="cart__popup__price"><span class="hider"><?= $arResult['TOTAL_PRICE'] ?> </span> ₽</span>
            </div>
            <a href="<?= $arParams['PATH_TO_ORDER'] ?>" class="btn-blue"><span>оформить заказ</span></a>
            <a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="btn-yellow"><span>перейти в корзину</span></a>
        </div>
    </div>
    <? ?>
    <script>
        BX.ready(function () {
            <?=$cartId?>.
            fixCart();
        });
    </script>
    <?
}