<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>


<div class="subscribe-section">


<?
$frame = $this->createFrame("subscribe-form", false)->begin();
?>
    <span class="label hidden-lg-up">Введи E-mail и начни получать самые свежие новости о скидках и акциях</span>
	<form action="<?=$arResult["FORM_ACTION"]?>">

	<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
		<label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
			<input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
		</label><br />
	<?endforeach;?>

        <div class="subscribe-form">
            <div class="text-wrap">
                <input type="text" name="sf_EMAIL" class="text-field hidden-md-down" value="<?=$arResult["EMAIL"]?>" placeholder="Введи E-mail и начни получать самые свежие новости о скидках и акциях">
                <input type="text" class="text-field hidden-lg-up" placeholder="E-mail">
            </div>
            <input type="submit" name="OK" class="submit" value="Отправить">
            <div class="corner-box corner-box_white"><div></div></div>
        </div>

	</form>
<?
$frame->end();
?>
</div>
