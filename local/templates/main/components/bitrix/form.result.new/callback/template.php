<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?=$arResult["FORM_HEADER"]?>

        <div class="text-field-wrap">
            <label for="form-name3"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_419"]["CAPTION"];?><sup>*</sup></label>
            <input type="text" class="text-field" id="form-name3" name="form_text_4">
        </div>
        <div class="text-field-wrap">
            <label for="form-phone3"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_424"]["CAPTION"];?><sup>*</sup></label>
            <input type="text" class="text-field phone-mask" id="form-phone3" name="form_text_5">
        </div>
        <div class="text-field-wrap">
            <label for="form-message3"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_307"]["CAPTION"];?><sup>*</sup></label>
            <textarea type="text" class="text-field text-field_last" id="form-message3" name="form_text_6"></textarea>
        </div>
        <span class="form-note"><?=GetMessage("FORM_APPLY_DATA");?></span>
        <div class="submit-wrap">
            <input class="submit btn-blue" type="submit" name="web_form_submit">
        </div>

<?=$arResult["FORM_FOOTER"]?>