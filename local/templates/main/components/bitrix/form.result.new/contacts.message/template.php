<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>


<?=$arResult["FORM_HEADER"]?>


    <div class="form_contacts">
        <div class="text-field-wrap">
            <label for="message-name"><?=GetMessage("FORM_Q_NAME")?><sup>*</sup></label>
            <input type="text" class="text-field" id="message-name" name="form_text_1" required="required">
        </div>
        <div class="text-field-wrap">
            <label for="message-phone"><?=GetMessage("FORM_Q_PHONE")?><sup>*</sup></label>
            <input type="text" class="text-field" id="message-phone" name="form_text_2" required="required">
        </div>
        <div class="text-field-wrap">
            <label style="position: relative"><?=GetMessage("FORM_Q_MESSAGE")?><sup>*</sup></label>
            <textarea cols="32" rows="12" name="form_textarea_3" required="required"></textarea>
        </div>
        <br>
        <div class="submit clearfix">
            <div class="pull-right">
                <input type="submit" name="iblock_submit" class="submit btn-blue" value="<?=GetMessage("FORM_Q_SUBMIT")?>" />
                <input type="hidden" name="web_form_apply" value="Y" />
            </div>
        </div>
        <p>
            <?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
        </p>
    </div>


<?=$arResult["FORM_FOOTER"]?>