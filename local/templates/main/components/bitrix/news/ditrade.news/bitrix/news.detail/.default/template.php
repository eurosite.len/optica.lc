<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="article-box animatedParent animateOnce">
    <div class="article-header animated fadeInUpShort">
        <span class="article-date"><?=FormatDate("d F Y", strtotime($arResult["TIMESTAMP_X"]))?></span>
        <div class="social-holder">
            <ul class="social-list">
                <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://ditrade2.extyl.pro/<?=$arResult["DETAIL_PAGE_URL"]?>" class="facebook">facebook</a>
                </li>
                <li>
                    <a href="https://vk.com/share.php?url=http://ditrade2.extyl.pro/<?=$arResult["DETAIL_PAGE_URL"]?>" class="vk">vk</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="animated fadeInUpShort">
        <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
            <p><?echo $arResult["DETAIL_TEXT"];?></p>
        <?else:?>
        <p><?echo $arResult["PREVIEW_TEXT"];?></p>
        <?endif?>
        <blockquote>
            <div>
                <q>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.</q>
            </div>
        </blockquote>
        <div class="content-divider"></div>
        <?if($arParams["USE_CATEGORIES"]):?>
            <div class="type-content">
                <h3><?= GetMessage("OUR_INTERESTING"); ?></h3>
                <div class="reviews-slider">
                    <?foreach($arResult["INTERESTING"] as $interesting):?>
                    <div>
                        <div class="article-item bg-image bg-image_native bg-image_right-bottom">
                            <?
                            if($interesting['PREVIEW_PICTURE'] != null) {
                                $image = CFile::ResizeImageGet($interesting['PREVIEW_PICTURE'], Array("width" => 420, "height" => 420), BX_RESIZE_IMAGE_EXACT);
                            } else {
//                                $image["src"] = "http://placehold.it/420x420";
                            }
                            ?>
                            <img src="<?=$image["src"]?>" alt="">
                            <div class="article-item__text-box">
                                <a href="<?="/".$interesting["IBLOCK_CODE"]."/".$interesting["CODE"]."/";?>" class="article-item__title"><?=$interesting["NAME"];?></a>
                                <p><?=$interesting["PREVIEW_TEXT"];?></p>
                            </div>
                            <? if ($interesting["TAGS"]): ?>
                                <div class="article-item__theme">
                                    <span class="article-item__label"><?= GetMessage("IN_ARTICLE"); ?></span>
                                    <span class="article-item__note"><?= $interesting["TAGS"] ?></span>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
            </div>
        <?endif;?>
        <div class="article-footer">
            <a href="<?=$arResult["PREW_ELEM"]["LINK"];?>" class="prev-article">
                <span class="title"><?= GetMessage("PREV_PAGE"); ?></span>
                <span class="sub-title"><?=$arResult["PREW_ELEM"]["NAME"];?></span>
            </a>
            <a href="<?=$arResult["NEXT_ELEM"]["LINK"];?>" class="next-article">
                <span class="title"><?= GetMessage("NEXT_PAGE"); ?></span>
                <span class="sub-title"><?=$arResult["NEXT_ELEM"]["NAME"];?></span>
            </a>
        </div>
    </div>
</div>

