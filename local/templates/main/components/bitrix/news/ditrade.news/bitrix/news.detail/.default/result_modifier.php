<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$i = 0;
	$arCollections = array();
	$objCollections = CIBlockElement::GetList( array($arParams["SORT_BY1"] => $arParams["SORT_ORDER1"], $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"]), array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "CODE"));
	while ( $array = $objCollections->Fetch() ) {
		$arCollections[$i] = $array;
		$arCollections[$i]["LINK"] = $arResult["LIST_PAGE_URL"] . $array["CODE"] . "/";
		$i++;
	}

	$count = count( $arCollections );
	foreach ( $arCollections as $key => $value ) {
		if ( $value["ID"] == $arResult["ID"] ) {
			$curElement = $key;
		}
	}

	if ( $curElement == 0 ) {
		$next = $arCollections[$curElement + 1]["LINK"];
		$name_next = $arCollections[$curElement + 1]["NAME"];
		$prew = false;
	} elseif ( $arCollections == $count - 1 ) {
		$next = false;
		$prew = $arCollections[$curElement - 1]["LINK"];
        $name_prew = $arCollections[$curElement - 1]["NAME"];
	} else {
		$next = $arCollections[$curElement + 1]["LINK"];
        $name_next = $arCollections[$curElement + 1]["NAME"];
		$prew = $arCollections[$curElement - 1]["LINK"];
        $name_prew = $arCollections[$curElement - 1]["NAME"];
	}

	$arResult["NEXT_ELEM"]["LINK"] = $next;
	$arResult["NEXT_ELEM"]["NAME"] = $name_next;
	$arResult["PREW_ELEM"]["LINK"] = $prew;
	$arResult["PREW_ELEM"]["NAME"] = $name_prew;

$arInteresting = CIBlockElement::GetList(
    array(
        $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
        $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"]
    ),
    array(
        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "PROPERTY_".$arParams["CATEGORY_CODE"] => $arResult["PROPERTIES"][$arParams["CATEGORY_CODE"]],
        "!ID" => $arResult["ID"],
    ),
    false,
    false,
    array(
        "ID",
        "NAME",
        "DETAIL_PAGE_URL",
        "PREVIEW_TEXT",
        "PREVIEW_PICTURE",
        "TAGS",
    )
);
$count = 0;
while(($interesting = $arInteresting->fetch()) && $count != $arParams["CATEGORY_ITEMS_COUNT"]) {
    $arResult["INTERESTING"][] = $interesting;
    $count++;
}
?>