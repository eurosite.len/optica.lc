<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>


<?if($_REQUEST["NEWS_FILTER_AJAX"] == "Y"){
    $APPLICATION->RestartBuffer();
}?>
    <div class="row article-list animatedParent animateOnce js_deleted">
        <svg version="1.1" id="article-set" class="animated" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 100"
             xml:space="preserve" width="80" height="100">
                <g>
                    <g>
                        <polygon class="triangle" stroke-opacity="null" stroke-width="1" stroke="#000"
                                 fill="#fff" points="0,40 25,40 26,0"/>
                    </g>
                </g>
            </svg>
        <svg id="lines" class="animated" xmlns="http://www.w3.org/2000/svg" width="100" height="36"
             viewBox="0 0 95.85 31.99">
            <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff"
                  d="M2.25,5.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,8.72,1.26,11-8.56,20.88-8.5C58.5,2.78,60.86,13,69.62,11.85c7.61-1,9.85-9.32,18-9.25A14.78,14.78,0,0,1,96,5.6"/>
            <path class="path2" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000"
                  fill="#fff"
                  d="M2.25,14.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,22,69.62,20.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
            <path class="path3" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000"
                  fill="#fff"
                  d="M2.25,24.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,9.22,1.33,10.77-8.56,20.88-8.5S60.86,32,69.62,30.85c7.61-1,9.85-9.32,18-9.25a14.78,14.78,0,0,1,8.38,3"/>
        </svg>
        <svg id="article-set2" class="animated" xmlns="http://www.w3.org/2000/svg" width="150" height="125"
             viewBox="0 0 150 125">
            <path class="line1 line" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000"
                  fill="#fff" d="M3,16.48,96.17,101.3" transform="translate(-2.66 -16.11)"/>
            <path class="line2 line" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000"
                  fill="#fff" d="M3,16.48,96.17,101.3" transform="translate(-2.66 -16.11)"/>
            <path class="line3 line" fill-opacity="0" stroke-opacity="null" stroke-width="2" stroke="#000"
                  fill="#fff" d="M3,16.48,96.17,101.3" transform="translate(-2.66 -16.11)"/>
            <circle class="pie1" cx="70" cy="70" r="18"/>
        </svg>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="col-12 col-md-6 col-lg-4 animated fadeInUpShort go"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="article-item bg-image bg-image_native bg-image_right-bottom">
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : /*"http://placehold.it/400x400"*/"" ?>"
                         alt="">
                    <div class="article-item__text-box">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                           class="article-item__title"><?= $arItem["NAME"] ?></a>
                        <p><?= $arItem["PREVIEW_TEXT"]; ?></p>
                    </div>
                    <? if ($arItem["FIELDS"]["TAGS"]): ?>
                        <div class="article-item__theme">
                            <span class="article-item__label"><?= GetMessage("IN_ARTICLE"); ?></span>
                            <span class="article-item__note"><?= $arItem["FIELDS"]["TAGS"] ?></span>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>
        <div class="col-12 col-md-6 col-lg-4 hidden-lg-up animated fadeInUpShort">
            <div class="article-item bg-image bg-image_native bg-image_right-bottom  animatedParent animateOnce">
                <img src="http://placehold.it/285x276" alt="">
                <div class="article-item__text-box">
                    <a href="#" class="article-item__title">Почему контактные линзы?</a>
                    <p>Откройте для себя свободу зрения, жизни и внешнего вида, которую предоставляют контактные
                        линзы, и начните наслаждаться жизнью без очков</p>
                </div>
                <div class="article-item__theme">
                    <span class="article-item__label">В статье:</span>
                    <span class="article-item__note">Наслаждение отличным зрением, Свобода от очков, Возможность отлично выглядеть, Защита от ультрафиолета</span>
                </div>
            </div>
        </div>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br class="js_deleted" /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
<?
if($_REQUEST["NEWS_FILTER_AJAX"] == "Y"){
    die();
}
?>

<?/*
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
						/></a>
			<?else:?>
				<img
					class="preview_picture"
					border="0"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					style="float:left"
					/>
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
			<?else:?>
				<b><?echo $arItem["NAME"]?></b><br />
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
	</p>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
*/?>