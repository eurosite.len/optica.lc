<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?if($USER->IsAuthorized()):?>
<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
<?elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && $_REQUEST["request"]=="Y" && empty($arResult["ERRORS"])):?>
	<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?else:?>
<?if($_REQUEST["request"]!="Y"):?>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" id="js_registrer">
<?endif;?>
		<?if (count($arResult["ERRORS"]) > 0):
		?><div class="error-holder"><?
			foreach ($arResult["ERRORS"] as $key => $error) {
				if (intval($key) == 0  && $key !=='LOGIN') {
					?><span class="error-text"><?=str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);?></span><?
				}
			}
			?>
		</div>
		<?endif?>
	<input type="text" name='REGISTER[LOGIN]' value="<?=$arResult["VALUES"]['LOGIN']?>" style="display: none" />
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['NAME'])>0):?>text text-active<?endif;?>">
		<label for="form-name"><?=GetMessage('NAME')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["NAME"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field" id="form-name" name='REGISTER[NAME]' value="<?=$arResult["VALUES"]['NAME']?>">
	</div>
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['LAST_NAME'])>0):?>text text-active<?endif;?>">
		<label for="form-lastname"><?=GetMessage('LAST_NAME')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["LAST_NAME"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field" id="form-lastname" name='REGISTER[LAST_NAME]' value="<?=$arResult["VALUES"]['LAST_NAME']?>">
	</div>
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['PASSWORD'])>0):?>text text-active<?endif;?>">
		<label for="form-pass2"><?=GetMessage('PASSWORD')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["PASSWORD"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field" id="form-pass2" name='REGISTER[PASSWORD]' value="<?=$arResult["VALUES"]['PASSWORD']?>">
	</div>
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['CONFIRM_PASSWORD'])>0):?>text text-active<?endif;?>">
		<label for="form-pass3"><?=GetMessage('CONFIRM_PASSWORD')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["CONFIRM_PASSWORD"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field" id="form-pass3" name='REGISTER[CONFIRM_PASSWORD]' value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>">
	</div>
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['EMAIL'])>0):?>text text-active<?endif;?>">
		<label for="form-email2"><?=GetMessage('EMAIL')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["EMAIL"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field" id="form-email2" name='REGISTER[EMAIL]' value="<?=$arResult["VALUES"]['EMAIL']?>">
	</div>
	<div class="text-field-wrap <?if(strlen($arResult["VALUES"]['PERSONAL_PHONE'])>0):?>text text-active<?endif;?>">
		<label for="form-phone"><?=GetMessage('PERSONAL_PHONE')?><?if($arResult["REQUIRED_FIELDS_FLAGS"]["PERSONAL_PHONE"]):?><sup>*</sup><?endif;?></label>
		<input type="text" class="text-field phone-mask text-field_last" id="form-phone" name='REGISTER[PERSONAL_PHONE]' value="<?=$arResult["VALUES"]['PERSONAL_PHONE']?>">
	</div>
	<?/* CAPTCHA */
	if ($arResult["USE_CAPTCHA"] == "Y")
	{
		?>
		<div class="text-field-wrap">
			<input type="hidden" class="text-field" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			</br>
			</br>
			<label  for="captcha_word"><?=GetMessage('REGISTER_CAPTCHA_PROMT')?><sup>*</sup></label>
			<input type="text" class="text-field"  id="form-capcha" name="captcha_word" maxlength="50" value="" />
		</div>
		<?
	}
	/* !CAPTCHA */
	?>
	<span class="form-note"><?=GetMessage("REGISTER_TEXT")?></span>
	<div class="submit-wrap">
		<!--<input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />-->
		<button class="submit btn-blue" type="button" onclick="registrer();"><span><?=GetMessage("AUTH_REGISTER")?></span></button>
	</div>
	<?//Параметр для вывода только соц сетей SOCIAL_CERVIS_REG
	$APPLICATION->IncludeComponent(
	   "bitrix:system.auth.form",
	   "main",
	   Array(
		   "FORGOT_PASSWORD_URL" => "",
		   "SOCIAL_CERVIS_REG" => "Y",
		   "PROFILE_URL" => "",
		   "REGISTER_URL" => "",
		   "SHOW_ERRORS" => "N"
	   )
   );?>
	<?if($_REQUEST["request"]!="Y"):?>
</form>
<?endif;?>

<?endif;?>
<script>
$(document).ready(function() {
    $(".phone-mask").mask("+7 (999) 999-9999");
});
</script>