function js_forgotpasswd(){
	var o = {};
	$('#js_forgotpasswd input.js_forgotpasswd').each(function () {
		o[$(this).attr('name')]=$(this).val();
	});
	o['forgotpasswd']='Y';
    jQuery(function ($) {
    $.ajax
		({
			type: "Post",
			url: "/ajax/forgotpasswd.php",
			data:o,
			dataType: "html",
			success: function(data)
			{
				$('#js_forgotpasswd').html(data);
			}
		});
    });
}