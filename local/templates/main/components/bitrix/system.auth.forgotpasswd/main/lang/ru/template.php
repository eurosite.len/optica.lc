<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите логин или E-Mail.<br />Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = "Выслать контрольную строку";
$MESS ['AUTH_GET_REPAIR'] = "Восстановление пароля";
$MESS ['AUTH_SEND'] = "Восстановить";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS ['AUTH_LOGIN'] = "Логин:";
$MESS ['AUTH_OR'] = "или";
$MESS ['AUTH_EMAIL'] = "Введите адрес электронной почты:";
$MESS["system_auth_captcha"] = "Введите символы с картинки:";

?>