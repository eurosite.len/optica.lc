<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset; ?>
    <!DOCTYPE HTML>
    <html lang="ru">
    <head>
        <? $APPLICATION->ShowHead(); ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? /************************************* [CSS] *******************************************/ ?>
        <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon.ico" type="image/x-icon">
        <?
        Asset::getInstance()->addCss("https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/ion.rangeSlider.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/animations.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/animations.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style-modal.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/all.css");
        ?>
        <? /************************************* [/CSS] *******************************************/ ?>
        <? /************************************* [JScript] ****************************************/ ?>
        <?
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-3.2.1.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.openclose.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/ion.rangeSlider.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/css3-animate-it.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.popups.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.accordion.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jcf.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jcf.select.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jcf.scrollable.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.tabs.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/cookie.js");
        ?>
        <? /************************************* [/JScript] ****************************************/ ?>
    </head>
<? $APPLICATION->ShowPanel() ?>
<body>
<div class="wrapper">
    <div class="header-wrap animatedParent animateOnce">
        <header class="header animated fadeInDownShort">
            <div class="container">
                <div class="top-line">
                    <? require_once($_SERVER["DOCUMENT_ROOT"] . "/include/header/GeoIP.php"); ?>
                    <? if ($USER->IsAuthorized()): ?>
                        <a href="?logout=yes" class="reg-link">выйти</a>
                        <a href="/personal/" class="login-link"><?= CUser::GetLogin(); ?></a>
                    <? else: ?>
                        <a href="#reg-popup" class="reg-link" data-fancybox>Регистрация</a>
                        <a href="#login-popup" class="login-link" data-fancybox>Войти</a>
                    <? endif; ?>
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "main_menu", Array(
                        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
                        "DELAY" => "N",    // Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",    // Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "COMPONENT_TEMPLATE" => "horizontal_multilevel"
                    ),
                        false
                    ); ?>
                </div>
                <div class="head-wrap">
                    <div class="head-contacts">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/include/header/phone.php"
                            )
                        ); ?>
                        <a href="#callback-popup" class="btn-blue callback"
                           data-fancybox><span>Обратный звонок</span></a>
                    </div>
                    <div class="head-cart">
                        <div class="wrap">
                            <a href="#" class="compare-link"></a>
                            <div class="cart-wrap">
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "head_basket", Array(
	"PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
		"PATH_TO_PERSONAL" => "/personal/",	// Страница персонального раздела
		"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
		"COMPONENT_TEMPLATE" => ".default",
		"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
		"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
		"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
		"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
		"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
		"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
		"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
		"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
		"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
		"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
		"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
		"POSITION_HORIZONTAL" => "right",
		"POSITION_VERTICAL" => "top",
		"HIDE_ON_BASKET_PAGES" => "N",	// Не показывать на страницах корзины и оформления заказа
		"SHOW_DELAY" => "Y",	// Показывать отложенные товары
		"SHOW_NOTAVAIL" => "Y",	// Показывать товары, недоступные для покупки
		"SHOW_IMAGE" => "Y",	// Выводить картинку товара
		"SHOW_PRICE" => "N",	// Выводить цену товара
		"SHOW_SUMMARY" => "Y",	// Выводить подытог по строке
	),
	false,
	array(
	"0" => ""
	)
);
                        ?>
                            </div>
                        </div>
                    <div class="search-form">
                            <form action="#">
                                <a href="#" class="search-form__opener"></a>
                                <div class="search-form__popup">
                                    <div class="text-wrap">
                                        <input type="text" class="text-field" placeholder="Что вы ищите?" id="tags">
                                    </div>
                                    <input type="submit" class="submit" value="Поиск">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="logo-wrap">
                        <span class="logo"><a
                                    href="<? if ($APPLICATION->GetCurPage() == "/"): ?>javascript:void(0)<? else: ?>/<? endif; ?>">Диоптрия</a><div
                                    class="corner-box"><div></div></div></span>
                        <div class="nav-wrap">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section.list",
                                "menu_top",
                                array(
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "COUNT_ELEMENTS" => "N",
                                    "IBLOCK_ID" => "15",
                                    "IBLOCK_TYPE" => "1c_catalog",
                                    "SECTION_CODE" => "",
                                    "SECTION_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "SECTION_ID" => "84",
                                    "SECTION_URL" => "/catalog/#CODE#/",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "SHOW_PARENT_NAME" => "Y",
                                    "TOP_DEPTH" => "0",
                                    "VIEW_MODE" => "TEXT",
                                    "COMPONENT_TEMPLATE" => "menu_top"
                                ),
                                false
                            ); ?>
                            <a href="#" class="nav-opener"><span></span></a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
<? if ($APPLICATION->GetCurPage() != '/'): ?>
    <div class="main main_inner">
    <div class="container">
    <div class="animatedParent animateOnce">
    <div class="page-nav">
        <h1><?= $APPLICATION->ShowTitle() ?></h1>
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "ditrade", Array(
            "PATH" => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
            "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
        ),
            false
        ); ?>
    </div>
    <? if ($APPLICATION->GetCurPage() != '/news/'): ?>
    </div>
<? endif; ?>
<? endif; ?>