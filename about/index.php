<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?>

    <div class="about-promo bg-image animatedParent animateOnce">
        <svg version="1.1" id="about-set" class="animated" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 100" xml:space="preserve" width="80" height="100">
                        <g>
                            <g>
                                <polygon class="triangle" stroke-opacity="null" stroke-width="1" stroke="#000" fill="#fff"  points="0,40 25,40 26,0"/>
                                <circle class="pie3" cx="40" cy="50" r="20"/>
                            </g>
                        </g>
                    </svg>
        <svg id="lines" class="animated" xmlns="http://www.w3.org/2000/svg" width="100" height="36" viewBox="0 0 95.85 31.99">
            <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M2.25,5.48a13.32,13.32,0,0,1,7.25-3c7.76-.51,10.36,7.64,18,8.75,8.72,1.26,11-8.56,20.88-8.5C58.5,2.78,60.86,13,69.62,11.85c7.61-1,9.85-9.32,18-9.25A14.78,14.78,0,0,1,96,5.6"/>
        </svg>
        <svg id="about-set2" class="animated" xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200">
            <path class="path" fill-opacity="0" stroke-opacity="null" stroke-width="3" stroke="#000" fill="#fff" d="M157,2V157H2V2Z"/>
            <path class="ellipse" fill-opacity="1" stroke-opacity="null" stroke-width="0" stroke="#000" fill="#000" d="M1754.81,790.567a2.613,2.613,0,0,1-3.61.875,2.66,2.66,0,0,1-.81-3.65,2.6,2.6,0,0,1,3.6-.875A2.662,2.662,0,0,1,1754.81,790.567Zm20,0a2.613,2.613,0,0,1-3.61.875,2.66,2.66,0,0,1-.81-3.65,2.6,2.6,0,0,1,3.6-.875A2.662,2.662,0,0,1,1774.81,790.567Zm20,0a2.613,2.613,0,0,1-3.61.875,2.66,2.66,0,0,1-.81-3.65,2.6,2.6,0,0,1,3.6-.875A2.662,2.662,0,0,1,1794.81,790.567Zm20,0a2.613,2.613,0,0,1-3.61.875,2.66,2.66,0,0,1-.81-3.65,2.6,2.6,0,0,1,3.6-.875A2.662,2.662,0,0,1,1814.81,790.567Zm-60,20a2.612,2.612,0,0,1-3.61.874,2.658,2.658,0,0,1-.81-3.649,2.6,2.6,0,0,1,3.6-.874A2.661,2.661,0,0,1,1754.81,810.567Zm20,0a2.612,2.612,0,0,1-3.61.874,2.658,2.658,0,0,1-.81-3.649,2.6,2.6,0,0,1,3.6-.874A2.661,2.661,0,0,1,1774.81,810.567Zm20,0a2.612,2.612,0,0,1-3.61.874,2.658,2.658,0,0,1-.81-3.649,2.6,2.6,0,0,1,3.6-.874A2.661,2.661,0,0,1,1794.81,810.567Zm20,0a2.612,2.612,0,0,1-3.61.874,2.658,2.658,0,0,1-.81-3.649,2.6,2.6,0,0,1,3.6-.874A2.661,2.661,0,0,1,1814.81,810.567Zm-3.8,21.228a2.6,2.6,0,1,1,3.61-.816A2.617,2.617,0,0,1,1811.01,831.8Zm0,19.942a2.6,2.6,0,1,1,3.61-.815A2.616,2.616,0,0,1,1811.01,851.737Zm0,19.943a2.6,2.6,0,1,1,3.61-.816A2.617,2.617,0,0,1,1811.01,871.68Zm0,19.942a2.6,2.6,0,1,1,3.61-.815A2.616,2.616,0,0,1,1811.01,891.622ZM1791.23,831.8a2.6,2.6,0,1,1,3.61-.816A2.614,2.614,0,0,1,1791.23,831.8Zm0,19.942a2.6,2.6,0,1,1,3.61-.815A2.614,2.614,0,0,1,1791.23,851.737Zm0,19.943a2.6,2.6,0,1,1,3.61-.816A2.614,2.614,0,0,1,1791.23,871.68Zm0,19.942a2.6,2.6,0,1,1,3.61-.815A2.614,2.614,0,0,1,1791.23,891.622Z"/>
        </svg>
        <img src="/html/images/img17.png" alt="">
        <div class="about-promo__content animated fadeInUpShort">
            <h2>Заголовок H2</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc_top",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
            <div class="animated-title animated">
                <a href="#" class="animated-title__text">все&nbsp;<span class="mark-blue">статьи</span></a>
                <div class="corner-box"><div></div></div>
            </div>
        </div>
    </div>
    <div class="type-content animatedParent animateOnce">
        <ul class="benefits-list animated fadeInUpShort">
            <li>
                <div class="icon-holder">
                    <img src="/html/images/ico-benefit01.png" alt="">
                    <div class="corner-box corner-box_yellow"><div></div></div>
                </div>
                <span class="benefits-list__title">Бесплатный подбор линз </span>
                <span class="benefits-list__text">и обучение их использованию</span>
            </li>
            <li>
                <div class="icon-holder">
                    <img src="/html/images/ico-benefit02.png" alt="">
                    <div class="corner-box corner-box_yellow"><div></div></div>
                </div>
                <span class="benefits-list__title">Быстрая доставка</span>
                <span class="benefits-list__text">в течение 3 часов</span>
            </li>
            <li>
                <div class="icon-holder">
                    <img src="/html/images/ico-benefit03.png" alt="">
                    <div class="corner-box corner-box_yellow"><div></div></div>
                </div>
                <span class="benefits-list__title">Гарантия высокого качества</span>
                <span class="benefits-list__text">на нашу продукцию</span>
            </li>
        </ul>
        <div class="animated fadeInUpShort">
            <h3>Заголовок H3</h3>
            <?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc_mid",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
        </div>
        <div class="reviews-section reviews-section_type animated fadeInUpShort">
            <div class="h_center reviews-section__title">
                <a href="#add_review-popup" class="btn-blue" data-fancybox><span>Оставьте ваш отзыв</span></a>
                <h4>Отзывы покупателей</h4>
            </div>
            <?global $review_block_filter;
            $review_block_filter = array(">PROPERTY_POST_REVIEW" => 0);
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "block_reviews",
                array(
                    "ACTIVE_DATE_FORMAT" => "j M Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "NAME",
                        1 => "PREVIEW_TEXT",
                        2 => "DATE_ACTIVE_FROM",
                        3 => "DATE_CREATE",
                        4 => "",
                    ),
                    "FILTER_NAME" => "review_block_filter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "services",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "280",
                    "PROPERTY_CODE" => array(
                        0 => "POST_REVIEW",
                        1 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => "block_reviews"
                ),
                false
            );?>
        </div>
    </div>
    <div class="promo-section">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6 col-xl-4">
                <div class="type-promo bg-image">
                    <img src="/html/images/img19.jpg" alt="">
                    <h3>Заголовок H3</h3>
                    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc_bot",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-5">
                <div class="sales-hit animatedParent animateOnce">
                    <span class="sales-hit__label">Количество линз в упаковке</span>
                    <div class="amount-nav slider-nav">
                        <div><span>6</span></a></div>
                        <div><span>12</span></a></div>
                        <div><span>24</span></a></div>
                        <div><span>30</span></a></div>
                    </div>
                    <div class="hit-slider slider-for">
                        <div>
                            <div class="item-box">
                                <div class="image-section">
                                    <a href="#"><img src="/html/images/img03.jpg" alt=""></a>
                                </div>
                                <div class="text-box">
                                    <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
                                    <div class="price-wrap">
                                        <span class="price-wrap__amount">6 линз</span>
                                        <span class="price-wrap__sep"></span>
                                        <span class="price-wrap__price">870 <span class="price-value">i</span></span>
                                    </div>
                                    <a href="#" class="btn-blue btn-large"><span>Купить</span></a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="item-box">
                                <div class="image-section">
                                    <a href="#"><img src="/html/images/img03.jpg" alt=""></a>
                                </div>
                                <div class="text-box">
                                    <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
                                    <div class="price-wrap">
                                        <span class="price-wrap__amount">12 линз</span>
                                        <span class="price-wrap__sep"></span>
                                        <span class="price-wrap__price">1870 <span class="price-value">i</span></span>
                                    </div>
                                    <a href="#" class="btn-blue btn-large"><span>Купить</span></a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="item-box">
                                <div class="image-section">
                                    <a href="#"><img src="/html/images/img03.jpg" alt=""></a>
                                </div>
                                <div class="text-box">
                                    <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
                                    <div class="price-wrap">
                                        <span class="price-wrap__amount">24 линз</span>
                                        <span class="price-wrap__sep"></span>
                                        <span class="price-wrap__price">3870 <span class="price-value">i</span></span>
                                    </div>
                                    <a href="#" class="btn-blue btn-large"><span>Купить</span></a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="item-box">
                                <div class="image-section">
                                    <a href="#"><img src="/html/images/img03.jpg" alt=""></a>
                                </div>
                                <div class="text-box">
                                    <a href="#" class="title">ACUVUE OASYS WITH HYDRACLEAR PLUS</a>
                                    <div class="price-wrap">
                                        <span class="price-wrap__amount">30 линз</span>
                                        <span class="price-wrap__sep"></span>
                                        <span class="price-wrap__price">6870 <span class="price-value">i</span></span>
                                    </div>
                                    <a href="#" class="btn-blue btn-large"><span>Купить</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="animated-title animated-title_large animated">
                        <a href="#" class="animated-title__text">оставить&nbsp;<span class="mark-blue">заявку</span></a>
                        <div class="corner-box"><div></div></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-12 col-xl-3">
                <div class="about-section bg-image bg-image_left-bottom about-section_single">
                    <img src="/html/images/img18.jpg" alt="">
                    <div class="about-section__holder">
                        <h2>АКСЕссуары</h2>
                        <div class="btn-bottom h_center">
                            <a href="#" class="btn-blue btn-large"><span>Перейти в каталог</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>