<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->IncludeComponent(
"bitrix:main.register",
"main",
Array(
    "AUTH" => "Y",
    "REQUIRED_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_PHONE"),
    "SET_TITLE" => "Y",
    "SHOW_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_PHONE"),
    "SUCCESS_PAGE" => "",
    "USER_PROPERTY" => array(),
    "USER_PROPERTY_NAME" => "",
    "USE_BACKURL" => "Y"
)
);?>